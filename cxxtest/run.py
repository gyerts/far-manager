import os

os.system('cls')

print('\n[1/3]: Generate CxxTest cpp files')
os.system('engine\bin\cxxtestgen --error-printer -o tests_for_project\generated_cpp\MyTestSuite.cpp tests_for_project\MyTestSuite.h')

print('\n\n[2/3]: Build CxxTest bin files')
os.system('g++ -o tests_for_project\generated_exe\MyTestSuite -I engine tests_for_project\generated_cpp\MyTestSuite.cpp')

print('\n\n[3/3]: Run CxxTest bin files')
os.system('tests_for_project\generated_exe\MyTestSuite.exe')

os.system('PAUSE')