class Airplane
{
public:
	Airplane(int numberOfSeats, double fuelTankCapacity)
	: m_numberOfSeats(numberOfSeats)
	, m_fuelTankCapacity(fuelTankCapacity)
	, m_fuel(0)
	{}

	double GetFuel() const 
	{ 
		return m_fuel; 
	}
	int GetFuelTankCapacity()
	{
		return m_fuelTankCapacity; 
	}
	double Refuel(double amountOfFuel)
	{
		double left = 0;

		double totalFuel = amountOfFuel + m_fuel;

		if( totalFuel > m_fuelTankCapacity ) 
		{
			m_fuel = m_fuelTankCapacity;
			left = totalFuel - m_fuelTankCapacity;
		}
		else
		{
			m_fuel = totalFuel;
		}
		return left;
	}

private:
	int m_numberOfSeats;
	double m_fuelTankCapacity;
	double m_fuel;
};