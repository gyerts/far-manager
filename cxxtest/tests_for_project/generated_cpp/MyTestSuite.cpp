/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#define _CXXTEST_HAVE_STD
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/ErrorPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::ErrorPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "cxxtest";
    status = CxxTest::Main< CxxTest::ErrorPrinter >( tmp, argc, argv );
    return status;
}
bool suite_AirplaneTestSuite_init = false;
#include "D:\Users\����\cppexam\cpp\cxxtest\tests_for_project\MyTestSuite.h"

static AirplaneTestSuite suite_AirplaneTestSuite;

static CxxTest::List Tests_AirplaneTestSuite = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_AirplaneTestSuite( "tests_for_project/MyTestSuite.h", 3, "AirplaneTestSuite", suite_AirplaneTestSuite, Tests_AirplaneTestSuite );

static class TestDescription_suite_AirplaneTestSuite_testAirplaneRefuel : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_AirplaneTestSuite_testAirplaneRefuel() : CxxTest::RealTestDescription( Tests_AirplaneTestSuite, suiteDescription_AirplaneTestSuite, 6, "testAirplaneRefuel" ) {}
 void runTest() { suite_AirplaneTestSuite.testAirplaneRefuel(); }
} testDescription_suite_AirplaneTestSuite_testAirplaneRefuel;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";
