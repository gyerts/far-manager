#include "Airplane.hpp"

class AirplaneTestSuite : public CxxTest::TestSuite 
{
public:
  void testAirplaneRefuel( void )
  {
      Airplane airplane(100, 500.0);
      TS_ASSERT_EQUALS(airplane.GetFuel(), 0);
      airplane.Refuel(100.0);
      TS_ASSERT_EQUALS(airplane.GetFuel(), 100.0);
      airplane.Refuel(200.0);
      TS_ASSERT_EQUALS(airplane.GetFuel(), 300.0);
      
      // количество топлива не может превышать топливный бак самолёта
      airplane.Refuel(300.0);
      TS_ASSERT_EQUALS(airplane.GetFuel(), airplane.GetFuelTankCapacity());
  }
};
