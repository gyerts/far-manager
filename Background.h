#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "Coord.h"
#include "Widget.h"
#include "ConsoleLib.h"
#include "PROGRAM_COLOR.h"

class Background : public Widget, public Observer
{
	//���������� Background
	Coord						m_coord;
	//������ � ������ Background
	int							m_width;
	int							m_height;

	STATE						m_state;
	COLOR::COLOR				m_color;

public:
	Background(Coord coord,	int width, int height, STATE st, COLOR::COLOR);
	virtual void Draw(STATE st);
	virtual RESULT HandleEvent(const INPUT_RECORD events);
	virtual void SetActivity(STATE st);
};

#endif /* BACKGROUND_H */
