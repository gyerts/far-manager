#ifndef MENU_H
#define MENU_H


#include "Button.h"
#include "Decorator.h"
#include "BorderOfFrame.h"
#include "Background.h"
#include <string>
#include <cstring>
#include "Coord.h"
#include "Observer.h"
#include "Option.h"
#include "LENTH.h"
#include "PROGRAM_COLOR.h"
#include "CONSOLE_COLOR.h"

using std::string;

class Menu
{
	int m_width;
	int m_heith;
	Coord m_coordOfWindow;
	Coord m_coordName;
	Coord m_coordInfo;

	Decorator* m_body;

	Button m_yes;
	Button m_no;
public:
	Menu(string btn1Name, string btn1Info, string btn2Name, string btn2Info) : 
		m_width(LENTH_TREE_ELEMENT_SIZE * 3),
		m_heith(QUANTITY_ELEM_OF_TREE_ON_PAGE / 3),
		m_coordOfWindow(Coord::MakeCoord(m_heith, LENTH_TREE_ELEMENT_SIZE / 2)),

		m_body(
			new BorderOfFrame(m_coordOfWindow, m_width, m_heith, 
				new Background(m_coordOfWindow, m_width, m_heith, DISABLED, COLOR::COLOR(Black)), DISABLED, COLOR::COLOR(LightGray))),

		m_yes(btn1Name, btn1Info, m_coordOfWindow.MoveDown(5).MoveRight(3), int(m_width / 2.5)), 
		m_no(btn2Name, btn2Info, m_coordOfWindow.MoveRight(int(m_width / 2.5 + 1)), int(m_width / 2.5)),
		m_coordName(m_coordOfWindow.MoveDown()),
		m_coordInfo(m_coordName.MoveDown())
	{

	}

	void Draw(string name, string info)
	{
		m_coordName.GoToBurnPosition();
		m_coordInfo.GoToBurnPosition();
		m_coordName.MoveDown(1);
		m_coordInfo.MoveDown(2);

		m_coordOfWindow.GoToBurnPosition();

		m_coordName.GoCenter(name, m_width);
		m_coordInfo.GoCenter(info, m_width);

		m_body->Draw(REDRAW);

		m_yes.Draw();
		m_no.Draw();

		SetColor(COLOR::TXT_ACTIVE, COLOR::BG_BACKGOUND);

		GotoXY(m_coordName);
		cout << name;
		GotoXY(m_coordInfo);
		cout << info;
	}

	RESULT Run()
	{
		INPUT_RECORD irInBuf[256];
		DWORD dwNumRead;
		Coord crd(0, 2);
		COORD mcrd;

		while(true)
		{
			ReadConsoleInput(hStdIn, irInBuf, 128, &dwNumRead);

			for (DWORD i = 0; i < dwNumRead; i++)
			{
				switch(irInBuf[i].EventType)
				{
				case MOUSE_EVENT:
					if(irInBuf[i].Event.MouseEvent.dwButtonState == FROM_LEFT_1ST_BUTTON_PRESSED)
					{
						mcrd = irInBuf[i].Event.MouseEvent.dwMousePosition;
						if(m_yes.AreYouHere(mcrd))
						{
							return YES;
						}
						else if(m_no.AreYouHere(mcrd))
						{
							return NO;
						}
					}
				case KEY_EVENT:
					if(!irInBuf[i].Event.KeyEvent.bKeyDown)
						switch(irInBuf[i].Event.KeyEvent.wVirtualKeyCode)
					{
						case VK_RETURN:
							return YES;
						case VK_ESCAPE:
							return NO;
					}
				}
			}
		}
		return NONE;
	}
};
#endif /* MENU_H */
