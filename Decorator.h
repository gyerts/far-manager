#ifndef DECORATOR_H
#define DECORATOR_H


#include "Observer.h"
#include "Widget.h"

class Decorator : public Widget
{
    Widget *wid;
public:
    Decorator(Widget *w);
	~Decorator();
    virtual void Draw(STATE st);
	virtual RESULT HandleEvent(const INPUT_RECORD events);
	virtual void SetActivity(STATE st);
};
#endif /* DECORATOR_H */
