#ifndef BEHAVIOUROFCLOSELEFT_H
#define BEHAVIOUROFCLOSELEFT_H


#include "Command.h"
#include "TotalCommander.h"

//��������� ������ �� ���������
class BehaviourOfCloseLeft : public Command
{
	TotalCommander* m_tc;
	Window* m_lwnd;
	Window* m_rwnd;
public:
	explicit BehaviourOfCloseLeft(TotalCommander* tc);
	virtual bool Do();
};
#endif /* BEHAVIOUROFCLOSELEFT_H */
