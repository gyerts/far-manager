#ifndef WINDOW_H
#define WINDOW_H


#include "IDrawable.h"
#include "Decorator.h"
#include "Tree.h"
#include "Coord.h"
#include "Background.h"
#include "BorderOfFrame.h"

class Window : public IDrawable, public Observer
{
private:
	//���������� ����
	Coord			m_coord;
	//������ � ������ ����
	int				m_width;
	int				m_height;
	//��������� ����
	Tree*			m_tree;
	Path*			m_path;
	Decorator*		m_cover;
	//��������� ����
	STATE			m_state;

public:
	Window(Coord crd, int wid, int hei, STATE st, Tree* tree, Decorator* cover, Path*);

	virtual void	Draw(STATE st);
	virtual RESULT	HandleEvent(const INPUT_RECORD events);
	void			SetActivity(STATE st);
	STATE			GetActivity();
	Tree*			GetTree();
	Path*			GetPath();
	string			GetPathBySelector();
};
#endif /* WINDOW_H */
