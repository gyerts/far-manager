#ifndef OBSERVABLE_H
#define OBSERVABLE_H


#include <list>
#include "Observer.h"

using std::list;

class Observable
{
	list<Observer*> m_observers;
public:
	void AddObserver(Observer*);
	void DeleteObserver(Observer*);
	void NotifyObservers(const INPUT_RECORD);
	void LetsGetUsersInput();
};
#endif /* OBSERVABLE_H */
