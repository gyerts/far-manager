#ifndef TREE_H
#define TREE_H


#include <utility>
#include <string>
#include <cstring>
#include "Option.h"
#include "Coord.h"
#include <windows.h>
#include <tchar.h>
#include <vector>
#include "Observer.h"
#include "Path.h"
#include "IDrawable.h"

using std::string;
using std::vector;

class Tree : public Observer, public IDrawable
{
	int															m_from;
	std::vector<int>											m_lastPositionOfFrom;
	int															m_prevFrom;
	bool														m_elJustDeleted;
	bool														m_makeDir;

public:
	std::vector<TYPE_OF_ELEMENT_TREE>							m_page_types;
	std::vector<std::string>									m_page_names;
	std::vector<int>											m_page_sizes;
	std::vector<bool>											m_page_selection;

private:
	int															m_selector;
	Coord														m_coord;
	std::string													m_filePath;
	char														m_drive;
	std::vector<int>											m_lastPositionOfSelector;
	STATE														m_state;
	int															m_width;
	Path*														m_path;

public:
	Tree(Coord coord, STATE st, int m_width, Path*);

	void				Draw(STATE);
	void				GoPath(const Path&);
	RESULT				HandleEvent(const INPUT_RECORD);
	void 				MoveSelector(WAY::WAY);
	void 				ClearPage();
	void 				BackUp();
	void 				ChngTxtAtr(bool);
	void 				GoInside();
	bool 				GoBack();
	bool 				AreYouHere(COORD);
	void 				Cls();
	void 				IJustDeleteElement();
	void 				ICreateFolder();
	void				SelectSelectedItem();

	void 				SetActivity(STATE st);
	void 				SetPathBySelector();

	std::string 		GetPathByMauseCoord(COORD mousePos);
	int					GetSelectorByMouseCoord(COORD mousePos);
	Coord 				GetCoord();
	int 				GetPageSize();
	const char* 		GetStrByType(TYPE_OF_ELEMENT_TREE) const;
	string 				GetPathBySelector();
	Path* 				GetPath();
	int 				GetSelector() const;
	Coord				GetCoord() const;
	int					GetWidth() const;
	vector<pair<string, TYPE_OF_ELEMENT_TREE> >*		GetVectorOfSelectedElements();
};
#endif /* TREE_H */
