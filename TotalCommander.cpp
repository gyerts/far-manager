#include "TotalCommander.h"

Command* TotalCommander::GetCommandPtr(COMMAND cmd)
{
	Command* res = NULL;
	vector<pair<Command *, COMMAND> >::iterator begin = m_commands.begin();
	vector<pair<Command *, COMMAND> >::iterator end = m_commands.end();

	while(begin != end)
	{
		if(begin->second == cmd)
		{
			res = begin->first;
			break;
		}
		++begin;
	}
	return res;
}
//-------------------------------------Observer--------------------------------------//
void TotalCommander::AddObserver(Observer* observer)
{
	m_observers.push_back(observer);
}
void TotalCommander::DeleteObserver(Observer* observer)
{
	m_observers.remove(observer);
}
void TotalCommander::NotifyObservers(const INPUT_RECORD events)
{
	list<Observer*>::iterator begin = m_observers.begin();
	list<Observer*>::iterator end = m_observers.end();

	while(begin != end)
	{
		(*begin)->HandleEvent(events);
		++begin;
	}
}
//-----------------------------------Constructors------------------------------------//
TotalCommander::TotalCommander() : m_buffer(NULL), m_window(new Window*[2]), m_toolBar(Coord::MakeCoord(QUANTITY_ELEM_OF_TREE_ON_PAGE + 2, 0))
{}
//----------------------------------GetterUserInput----------------------------------//
void TotalCommander::LetsGetUsersInput()
{
	Draw();

	DWORD dwOldMode, dwMode;
	HANDLE hStdIn = GetStdHandle(STD_INPUT_HANDLE);

	// ��������� ������� ����� ����� ��� �������� �������������� ��� ������ �� ���������
	GetConsoleMode(hStdIn, &dwOldMode);
	// �������� ��������� ������� �� ����
	dwMode = ENABLE_MOUSE_INPUT;
	SetConsoleMode(hStdIn, dwMode);

	INPUT_RECORD						irInBuf[128];
	DWORD								dwNumRead;

	BOOL keyIsDown;
	WORD keyVitrualCode;

	static DWORD mouseButtonState;
	static COORD mousePos;

	while(true)
	{
		ReadConsoleInput(hStdIn, irInBuf, 128, &dwNumRead);

		for (DWORD i = 0; i < dwNumRead; i++)
		{
			keyIsDown = irInBuf[i].Event.KeyEvent.bKeyDown;
			keyVitrualCode = irInBuf[i].Event.KeyEvent.wVirtualKeyCode;

			switch(irInBuf[i].EventType)
			{
			case MOUSE_EVENT:
				mouseButtonState = irInBuf[i].Event.MouseEvent.dwButtonState;
				mousePos = irInBuf[i].Event.MouseEvent.dwMousePosition;

				if (mouseButtonState == FROM_LEFT_1ST_BUTTON_PRESSED)
				{
					//���� ���� � ����� � ����� �����
					if (irInBuf[i].Event.MouseEvent.dwEventFlags == DOUBLE_CLICK)
					{
						m_mouseCoord = mousePos;
						DoCommand(OPEN_FOLDER);
					}
					//����� ������ �������� �� ��������� ���� � ���� �� ������ ����������
					else
					{
						if(m_window[LEFT]->GetTree()->AreYouHere(mousePos))
						{
							if(m_window[LEFT]->GetActivity() == DISACTIVATED)
							{
								if(m_window[RIGHT]->GetActivity()==ACTIVATED)
								{
									m_window[RIGHT]->SetActivity(DISACTIVATED);
									m_window[LEFT]->SetActivity(ACTIVATED);
									DeleteObserver(m_window[RIGHT]);
									AddObserver(m_window[LEFT]);
								}
							}
							else
							{
								if((irInBuf[i].Event.MouseEvent.dwControlKeyState & LEFT_CTRL_PRESSED) || (irInBuf[i].Event.MouseEvent.dwControlKeyState & RIGHT_CTRL_PRESSED))
								{
									m_window[LEFT]->GetTree()->SelectSelectedItem();
									m_window[LEFT]->GetTree()->Draw(ACTIVATED);
								}
							}
						}
						else if(m_window[RIGHT]->GetTree()->AreYouHere(mousePos))
						{
							if(m_window[RIGHT]->GetActivity() == DISACTIVATED)
							{
								if(m_window[LEFT]->GetActivity()==ACTIVATED)
								{
									m_window[LEFT]->SetActivity(DISACTIVATED);
									m_window[RIGHT]->SetActivity(ACTIVATED);
									DeleteObserver(m_window[LEFT]);
									AddObserver(m_window[RIGHT]);
								}
							}
							else
							{
								if((irInBuf[i].Event.MouseEvent.dwControlKeyState & LEFT_CTRL_PRESSED) || (irInBuf[i].Event.MouseEvent.dwControlKeyState & RIGHT_CTRL_PRESSED))
								{
									m_window[RIGHT]->GetTree()->SelectSelectedItem();
									m_window[RIGHT]->GetTree()->Draw(ACTIVATED);
								}
							}
						}
					}
				}
			case KEY_EVENT:
				if (!keyIsDown)
				{
					switch (keyVitrualCode)
					{
					case VK_TAB:
						DoCommand(GO_TO_OTHER_WINDOW);
						break;
					case VK_F1:
						DoCommand(CLOSE_LEFT);
						break;
					case VK_F2:
						DoCommand(CLOSE_RIGHT);
						break;
					case VK_F5:
						if(irInBuf[i].Event.KeyEvent.dwControlKeyState & SHIFT_PRESSED)
							DoCommand(COPY);
						break;
					case VK_F6:
						if(irInBuf[i].Event.KeyEvent.dwControlKeyState & SHIFT_PRESSED)
							DoCommand(PASTE);
						break;
					case VK_F4:
						DoCommand(MAKE_DIR);
						break;
					case VK_DELETE:
					case VK_F8:
						if(irInBuf[i].Event.KeyEvent.dwControlKeyState & LEFT_CTRL_PRESSED)
							DoCommand(DELETE_ITEM);
						break;
					case VK_ESCAPE:
						DoCommand(EXIT);
						break;
					}
				}
				NotifyObservers(irInBuf[i]);
			}
		}
	}
}
//--------------------------------------Strategy-------------------------------------//
bool TotalCommander::DoCommand(COMMAND cmd)
{
	Command* temp = GetCommandPtr(cmd);
	if(temp)
	{
		return temp->Do();
	}
	else
	{
		return false;
	}
}
bool TotalCommander::AddCommand(Command* cmd, COMMAND enm)
{
	pair<Command*, COMMAND> pr;
	pr.first = cmd;
	pr.second = enm;

	m_commands.push_back(pr);
	return true;
}
//---------------------------------------Set&Get-------------------------------------//
Window* TotalCommander::GetActiveWindow()
{
	Window* res;
	if(m_window[LEFT]->GetActivity() == ACTIVATED) res = m_window[LEFT];
	else if(m_window[RIGHT]->GetActivity() == ACTIVATED) res = m_window[RIGHT];
	else res = NULL;

	return res;
}
void TotalCommander::Draw()
{
	m_window[0]->Draw(ACTIVATED);
	m_window[1]->Draw(DISACTIVATED);
	m_toolBar.Draw(0);
}
Window* TotalCommander::GetWindow(SIDE sd)
{
	return m_window[sd];
}
COORD TotalCommander::GetMouseCoord()
{
	return m_mouseCoord;
}
bool TotalCommander::SetBuffer(vector<pair<string, TYPE_OF_ELEMENT_TREE> >* bf)
{
	m_buffer = bf;
	return true;
}
vector<pair<string, TYPE_OF_ELEMENT_TREE> >* TotalCommander::GetBuffer()
{
	return m_buffer;
}
bool TotalCommander::CleanBuffer()
{
	if(m_buffer)
	{
		delete m_buffer;
		m_buffer = NULL;
	}
	return true;
}
void TotalCommander::SetBufferPuth(const string& path)
{
	m_buffer_path.clear();
	m_buffer_path = path;
}
const string& TotalCommander::GetBufferPuth()
{
	return m_buffer_path;
}
TotalCommander* TotalCommander::GetInstance()
{
	if(!m_instance)
	{
		m_instance = new TotalCommander;
	}
	return m_instance;
}
void TotalCommander::SetWindows(Window* left, Window* right)
{
	m_window[LEFT] = left;
	m_window[RIGHT] = right;
	AddObserver(m_window[LEFT]);
	AddObserver(&m_toolBar);
}
