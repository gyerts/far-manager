#ifndef WIDGET_H
#define WIDGET_H


#include "Observer.h"
#include "Option.h"
#include "IDrawable.h"

class Widget : public IDrawable
{
public:
	virtual void SetActivity(STATE st) = 0;
	virtual RESULT HandleEvent(const INPUT_RECORD events) = 0;
};
#endif /* WIDGET_H */
