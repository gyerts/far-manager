#ifndef BEHAVIOUROFCOPY_H
#define BEHAVIOUROFCOPY_H


#include "Command.h"
#include "TotalCommander.h"

//��������� ������ �� ���������
class BehaviourOfCopy : public Command
{
	TotalCommander* m_tc;
public:
	BehaviourOfCopy();
	explicit BehaviourOfCopy(TotalCommander* tc);
	virtual bool Do();
};
#endif /* BEHAVIOUROFCOPY_H */
