#include <iostream>
#include <windows.h>
#include <vector>

#include "ConsoleLib.h"

using std::cout;
using std::endl;

// ������ �������
HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
HANDLE hStdIn = GetStdHandle(STD_INPUT_HANDLE);

// ����������/������ ��������� ������
void ShowCursor(bool visible)
{
	CONSOLE_CURSOR_INFO cci = { sizeof(cci), visible };
	SetConsoleCursorInfo(hStdOut, &cci);
}

// ������������� ���� �������� � ����
void SetColor(ConsoleColor text, ConsoleColor background)
{
	SetConsoleTextAttribute(hStdOut, (WORD)((background << 4) | text));
}
void SetColor(COLOR::COLOR text, COLOR::COLOR background)
{
	SetConsoleTextAttribute(hStdOut, (WORD)((background << 4) | text));
}
void SetColor(ConsoleColor text)
{
	SetConsoleTextAttribute(hStdOut, text);
}

// ���������� ������ � �������� �������
void GotoXY(Coord &coord)
{
	COORD crd = {coord.GetY(), coord.GetX()};
	SetConsoleCursorPosition(hStdOut, crd);
}

// ������� �������� ������ � �������� �������
void WriteStr(int X, int Y, const char *Str)
{
	//  GotoXY(X, Y);
	cout << Str << std::flush;
}

// ������� �������� ������ ������� � �������� �������
void WriteChar(int X, int Y, char Ch)
{
	//GotoXY(X, Y);
	cout << Ch;
}

// ������� ��������� ���������� �������� �������� ������� � �������� �������
void WriteChars(int X, int Y, char Ch, unsigned Len)
{
	//GotoXY(X, Y);
	for (unsigned i = 0; i < Len; i++)
		cout << Ch;
}

// ������ ��������� ���������, ������� � �������� �������
void ChangeTextAttr(int X, int Y, ConsoleColor text, ConsoleColor background, unsigned Len)
{
	COORD coord = { X, Y };
	DWORD dwDummy;
	FillConsoleOutputAttribute(hStdOut, (WORD)((background << 4) | text), Len, coord, &dwDummy);
}
void ChangeTextAttr(int X, int Y, COLOR::COLOR text, COLOR::COLOR background, unsigned Len)
{
	COORD coord = { X, Y };
	DWORD dwDummy;
	FillConsoleOutputAttribute(hStdOut, (WORD)((background << 4) | text), Len, coord, &dwDummy);
}
void ChangeTextAttr(int X, int Y, COLOR::COLOR background, unsigned Len)
{
	COORD coord = { X, Y };
	DWORD dwDummy;
	FillConsoleOutputAttribute(hStdOut, (WORD)(background << 4), Len, coord, &dwDummy);
}
void ChangeTextAttr(Coord crd, int selector, int from, std::vector<TYPE_OF_ELEMENT_TREE>& types, bool selected, std::vector<std::string>& names)
{
	crd.MoveDown(selector);
	COORD coord = { crd.GetY(), crd.GetX() };
	DWORD dwDummy;

	COLOR::COLOR cl;
	COLOR::COLOR txt;

	if(selected) 
	{
		cl = COLOR::BG_OF_SELECTED_EL_IN_TREE;
		txt = COLOR::TXT_ACTIVE;
	}
	else 
	{
		cl = COLOR::BG_OF_WINDOW;
		txt = COLOR::TXT_OF_FILE_NAME;
	}

	static int bigger = 4;

	int size = names[selector + from].size() > LENTH_TREE_ELEMENT_NAME - bigger?LENTH_TREE_ELEMENT_NAME - bigger:names[selector + from].size();

	if(types[selector + from] == FOLDER_tree)
	{
		//���
		FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | COLOR::TXT_OF_FOLDER_NAME), size + 1, coord, &dwDummy);
		if(size == LENTH_TREE_ELEMENT_NAME - bigger)
		{
			coord.X += (size + 1);
			FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | COLOR::COLOR(Red)), bigger - 1, coord, &dwDummy);
			coord.X += (bigger - 1);
		}
		else
		{
			coord.X += size;
			FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | cl), LENTH_TREE_ELEMENT_NAME - size, coord, &dwDummy);
			coord.X += LENTH_TREE_ELEMENT_NAME - size;
		}


		//���
		FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | COLOR::TXT_OF_FOLDER_TYPE), LENTH_TREE_ELEMENT_TYPE, coord, &dwDummy);
		coord.X += LENTH_TREE_ELEMENT_TYPE;

		//������
		FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | cl), LENTH_TREE_ELEMENT_SIZE, coord, &dwDummy);
	}
	else if(types[selector + from] == FILE_tree)
	{
		//���
		FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | txt), size, coord, &dwDummy);
		
		//��� -> ���
		if(size == LENTH_TREE_ELEMENT_NAME - bigger)
		{
			coord.X += size;
			FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | COLOR::COLOR(Red)), bigger, coord, &dwDummy);
			coord.X += bigger;
		}
		else
		{
			coord.X += size;
			FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | cl), LENTH_TREE_ELEMENT_NAME - size, coord, &dwDummy);
			coord.X += LENTH_TREE_ELEMENT_NAME - size;
		}

		//���
		FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | txt), LENTH_TREE_ELEMENT_TYPE, coord, &dwDummy);
		coord.X += LENTH_TREE_ELEMENT_TYPE;

		//������
		FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | txt), LENTH_TREE_ELEMENT_SIZE, coord, &dwDummy);
	}
	else if(types[selector + from] == RETURN_tree)
	{
		//���
		FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | COLOR::TXT_OF_FOLDER_NAME), size, coord, &dwDummy);
		
		//��� -> ���
		if(size == LENTH_TREE_ELEMENT_NAME - bigger)
		{
			coord.X += size;
			FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | COLOR::COLOR(Red)), bigger, coord, &dwDummy);
			coord.X += bigger;
		}
		else
		{
			coord.X += size;
			FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | cl), LENTH_TREE_ELEMENT_NAME - size, coord, &dwDummy);
			coord.X += LENTH_TREE_ELEMENT_NAME - size;
		}

		//���
		FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | cl), LENTH_TREE_ELEMENT_TYPE, coord, &dwDummy);
		coord.X += LENTH_TREE_ELEMENT_TYPE;

		//������
		FillConsoleOutputAttribute(hStdOut, (WORD)((cl << 4) | cl), LENTH_TREE_ELEMENT_SIZE, coord, &dwDummy);
	}
}