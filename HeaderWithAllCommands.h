#ifndef HEADERWITHALLCOMMANDS_H
#define HEADERWITHALLCOMMANDS_H

#include "BehaviourOfCloseLeft.h"
#include "BehaviourOfExit.h"
#include "BehaviourOfCloseRight.h"
#include "BehaviourOfChangeActiveWindow.h"
#include "BehaviourOfCopy.h"
/*#include "BehaviourOfPaste.h"*/
#include "BehaviourOfMove.h"
#include "BehaviourOfRead.h"
/*#include "BehaviourOfMakeDir.h"
#include "BehaviourOfDelete.h"*/
#include "BehaviourOfChoiseDriveLeft.h"
#include "BehaviourOfChoiseDriveRight.h"

#endif /* HEADERWITHALLCOMMANDS_H */
