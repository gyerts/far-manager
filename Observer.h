#ifndef OBSERVER_H
#define OBSERVER_H


#include <windows.h>
#include "Option.h"

class Observer
{
public:
	virtual RESULT HandleEvent(const INPUT_RECORD) = 0;
};
#endif /* OBSERVER_H */
