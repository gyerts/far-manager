#ifndef BUTTON_H
#define BUTTON_H


#include "Coord.h"
#include <string>
#include <cstring>
#include "Option.h"
#include "ConsoleLib.h"
#include <iostream>
#include "BorderOfFrame.h"
#include "Background.h"


using std::cout;
using std::string;

class Button
{
	string		m_name;
	string		m_info;
	Coord		m_coord;

	int			m_width;
	Decorator*  m_decorator;

public:
	explicit	Button(string name, string info, Coord crd, int w) : m_name(name), m_info(info), m_coord(crd), m_width(w), m_decorator(new BorderOfFrame(m_coord, m_width, 4, new Background(m_coord, m_width, 4, DISABLED, COLOR::BG_BUTTON), DISABLED, COLOR::BG_BUTTON)){}
	void		Draw()
	{
		m_decorator->Draw(REDRAW);
		int dif;
		setlocale(0, "");
		m_coord.GoToNextLine();
		dif = (m_width - m_name.size()) / 2;
		m_coord.MoveRight();
		ChangeTextAttr(m_coord.GetY(), m_coord.GetX(), COLOR::BG_BUTTON, COLOR::BG_BUTTON, dif);//
		GotoXY(m_coord.MoveRight(dif - 1));
		cout << m_name;
		m_coord.MoveRight(m_name.size());
		ChangeTextAttr(m_coord.GetY(), m_coord.GetX(), COLOR::BG_BUTTON, COLOR::BG_BUTTON, m_width - 1 - dif - m_name.size());//

		SetColor(LightGray, LightBlue);
		m_coord.GoToNextLine();
		dif = (m_width - m_info.size()) / 2;
		m_coord.MoveRight();
		ChangeTextAttr(m_coord.GetY(), m_coord.GetX(), COLOR::BG_BUTTON, COLOR::BG_BUTTON, dif);//
		GotoXY(m_coord.MoveRight(dif - 1));
		cout << m_info;
		m_coord.MoveRight(m_info.size());
		ChangeTextAttr(m_coord.GetY(), m_coord.GetX(), COLOR::BG_BUTTON, COLOR::BG_BUTTON, m_width - 1 - dif - m_info.size());//

		m_coord.GoToBurnPosition();
	}
	bool		AreYouHere(COORD crd)
	{
		bool res = false;
		if(crd.X > m_coord.GetY() && crd.X < m_width + m_coord.GetY())
		{
			if(crd.Y > m_coord.GetX() && crd.Y < 4 + m_coord.GetX())
			{
				res = true;
			}
		}

		return res;
	}
};


#endif /* BUTTON_H */
