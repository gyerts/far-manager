#ifndef BEHAVIOUROFMAKEDIR_H
#define BEHAVIOUROFMAKEDIR_H


#include "Command.h"
#include "TotalCommander.h"

//��������� ������ �� ���������
class BehaviourOfMakeDir : public Command
{
	TotalCommander* m_tc;
public:
	BehaviourOfMakeDir();
	explicit BehaviourOfMakeDir(TotalCommander* tc);
	virtual bool Do();
};
#endif /* BEHAVIOUROFMAKEDIR_H */
