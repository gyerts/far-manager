#ifndef WAY_H
#define WAY_H


namespace WAY
{
	enum WAY
	{
		UP,
		DOWN, 
		LEFT, 
		RIGHT
	};
};
#endif /* WAY_H */
