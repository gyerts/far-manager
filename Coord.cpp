#include "Coord.h"
#include <iostream>

//����������� ����������� ��� ����������
Coord::Coord(int x, int y):X(x),temp_X(x), Y(y), temp_Y(y), lines(0){}
//���������� �� ���������, ���������� ����������� ����
Coord::Coord():X(0),temp_X(0), Y(0), temp_Y(0), lines(0){}
//������� ��������� ������ ���������, � ���������� ���
Coord Coord::MakeCoord(int x, int y)
{
	return Coord(x, y);
}
//����� ���������� ��������� ���������� �� ������� �����
Coord& Coord::MoveLeft()
{
	temp_Y--;
	return *this;
}
Coord& Coord::MoveLeft(int q)
{
	temp_Y -= q;
	return *this;
}
//����� ���������� ��������� ���������� �� ������� ������
Coord& Coord::MoveRight()
{
	temp_Y++;
	return *this;
}
//����� ���������� ��������� ���������� �� ������� �����
Coord& Coord::MoveUp()
{
	temp_X--;
	return *this;
}
Coord& Coord::MoveUp(int q)
{
	temp_X -= q;
	return *this;
}
//����� ���������� ��������� ���������� �� ������� ����
Coord& Coord::MoveDown()
{
	temp_X++;
	return *this;
}
Coord& Coord::MoveDown(int q)
{
	temp_X += q;
	return *this;
}
//����� ���������� ��������� ��������� � ����� ����� ������� �� ����� ������
Coord& Coord::GoToNextLine()
{
	lines++;
	temp_X = X + lines;
	temp_Y = Y;
	return *this;
}
//����� ��������� ���������� ��������� X
int Coord::GetX() const
{
	return temp_X;
}
//����� ��������� ���������� ��������� Y
int Coord::GetY() const
{
	return temp_Y;
}
//������� � ������� ��� ������ ������� ����� �������� �� ����� ����� � ������ ����������
Coord& Coord::GoCenter(const string& str, const pair<UINT, UINT>& width)
{
	temp_Y += ((width.first / 2) - (str.size() / 2));
	return *this;
}
Coord& Coord::GoCenter(const string& str, const int width)
{
	temp_Y += (width - str.size()) / 2;
	return *this;
}
//���������� �������������� �������� ���������
void Coord::GoToBurnPosition()
{
	temp_X = X;
	temp_Y = Y;
	lines = 0;
}
Coord& Coord::operator+=(int width)
{
	temp_Y+= width;
	return *this;
}
Coord::Coord(const Coord& crd) : 
	lines(0), 
	X(crd.temp_X), 
	Y(crd.temp_Y), 
	temp_X(crd.temp_X), 
	temp_Y(crd.temp_Y){}
Coord& Coord::MoveRight(int q)
{
	temp_Y += q;
	return *this;
}