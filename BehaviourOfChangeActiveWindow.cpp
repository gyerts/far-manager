#include "BehaviourOfChangeActiveWindow.h"
#include <iostream>

using std::cout;
using std::endl;

BehaviourOfChangeActiveWindow::BehaviourOfChangeActiveWindow(TotalCommander* tc):
	m_tc(tc),
	m_lwnd(m_tc->GetWindow(m_tc->LEFT)),
	m_rwnd(m_tc->GetWindow(m_tc->RIGHT)){}
bool BehaviourOfChangeActiveWindow::Do()
{
	Window* lwnd = m_tc->GetWindow(m_tc->LEFT);
	Window* rwnd = m_tc->GetWindow(m_tc->RIGHT);

	if(m_lwnd->GetActivity() == ACTIVATED && m_rwnd->GetActivity() == DISACTIVATED)
	{
		m_lwnd->SetActivity(DISACTIVATED);
		m_rwnd->SetActivity(ACTIVATED);

		m_tc->DeleteObserver(m_lwnd);
		m_tc->AddObserver(m_rwnd);
	}
	else if(m_rwnd->GetActivity() == ACTIVATED && m_lwnd->GetActivity() == DISACTIVATED)
	{
		m_lwnd->SetActivity(ACTIVATED);
		m_rwnd->SetActivity(DISACTIVATED);

		m_tc->DeleteObserver(m_rwnd);
		m_tc->AddObserver(m_lwnd);
	}
	return true;
}