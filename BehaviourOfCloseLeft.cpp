#include "BehaviourOfCloseLeft.h"

BehaviourOfCloseLeft::BehaviourOfCloseLeft(TotalCommander* tc):
	m_tc(tc),
	m_lwnd(m_tc->GetWindow(m_tc->LEFT)),
	m_rwnd(m_tc->GetWindow(m_tc->RIGHT)){}
bool BehaviourOfCloseLeft::Do()
{
	switch(m_lwnd->GetActivity())
	{
	case ACTIVATED:
	case DISACTIVATED:
		m_lwnd->SetActivity(DISABLED);
		m_tc->DeleteObserver(m_lwnd);
		if(m_rwnd->GetActivity() == DISACTIVATED)
		{
			m_rwnd->SetActivity(ACTIVATED);
			m_tc->AddObserver(m_rwnd);
		}
		break;
	case DISABLED:
		if(m_rwnd->GetActivity() == ACTIVATED)
			m_lwnd->SetActivity(DISACTIVATED);
		else
		{
			m_lwnd->SetActivity(ACTIVATED);
			m_tc->AddObserver(m_lwnd);
		}
	}
	return true;
}