#ifndef COORD_H
#define COORD_H


#include <string>
#include <windows.h>
#include <cstring>
using std::string;
using std::pair;

class Coord
{
	int lines;
	int temp_X;
	int temp_Y;
	int X;
	int Y;
public:
	Coord(int x, int y);
	Coord();
	Coord(const Coord&);

	Coord& GoCenter(const string&, const pair<UINT, UINT>&);
	Coord& MoveLeft();
	Coord& MoveLeft(int);
	Coord& MoveRight();
	Coord& MoveRight(int);
	Coord& MoveUp();
	Coord& MoveUp(int);
	Coord& MoveDown();
	Coord& MoveDown(int);
	Coord& GoToNextLine();
	static Coord MakeCoord(int, int);
	int GetX() const;
	int GetY() const;
	void GoToBurnPosition();
	Coord& operator+=(int);
	Coord& GoCenter(const string& str, const int width);
};
#endif /* COORD_H */
