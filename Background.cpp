#include "Background.h"
#include <iostream>
using std::cout;

Background::Background(Coord coord,	int width, int height, STATE st, COLOR::COLOR cl)
	:m_coord(coord), m_width(width), m_height(height), m_state(st), m_color(cl){}
void Background::Draw(STATE st)
{
	switch(st)
	{
	case REDRAW:
		{
			int x = m_coord.GetX();
			int y = m_coord.GetY();
			for(int i = 0; i < m_height; i++)
			{
				ChangeTextAttr(y, x + i, m_color, m_color, m_width);
			}
			st = ACTIVATED;
		}
		break;
	case ACTIVATED:
	case DISACTIVATED:
		if(m_state == ACTIVATED || m_state == DISACTIVATED);

		else
		{
			int x = m_coord.GetX();
			int y = m_coord.GetY();
			for(int i = 0; i < m_height; i++)
			{
				ChangeTextAttr(y, x + i, m_color, m_color, m_width);
			}
		}
		break;

	case DISABLED:
		for(int i = 0; i < m_height; ++i)
		{
			GotoXY(m_coord);
			ChangeTextAttr(m_coord.GetY(), m_coord.GetX(), 
				COLOR::BG_BACKGOUND,
				COLOR::BG_BACKGOUND,
				//+8 �������� �� ��� ����� <folder> ���� ���� �������
				m_width);
			m_coord.GoToNextLine();
		}
		m_coord.GoToBurnPosition();

		break;
	}
	m_state = st;
}
RESULT Background::HandleEvent(const INPUT_RECORD events)
{
	setlocale(0, "");

	DWORD mouseButtonState;

	WORD keyState;

	switch(events.EventType)
	{
	case KEY_EVENT:
		keyState = events.Event.KeyEvent.dwControlKeyState;

		if(events.Event.KeyEvent.wVirtualKeyCode == VK_F1)
			if(keyState & LEFT_CTRL_PRESSED || keyState & RIGHT_CTRL_PRESSED)
			{
				//Hide();
			}

	case MOUSE_EVENT:
		mouseButtonState = events.Event.MouseEvent.dwButtonState;
		switch(mouseButtonState)
		{
		case RIGHTMOST_BUTTON_PRESSED:
			GotoXY(m_coord);
			cout << "Beckgound ���� �������� ��������� ������ ����";
			break;
		}
		break;
	}
	return NONE;
}
void Background::SetActivity(STATE st)
{
	m_state = st;
}