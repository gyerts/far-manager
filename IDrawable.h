#ifndef IDRAWABLE_H
#define IDRAWABLE_H

#include "Option.h"

class IDrawable
{
public:
	virtual void Draw(STATE) = 0;
};

#endif /* IDRAWABLE_H */
