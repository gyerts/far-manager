#include "Window.h"
#include <iostream>
using std::cout;

Window::Window(Coord crd, int wid, int hei, STATE st, Tree* tree, Decorator* cover, Path* pth)
	:
	m_coord(crd),
	m_width(wid),
	m_height(hei),
	m_state(st),
	m_tree(tree),
	m_cover(cover)
{
	m_path = pth;
}
void Window::Draw(STATE st)
{
	m_cover->Draw(st);
	m_tree->Draw(st);
	
	if(st == REDRAW) m_state = ACTIVATED;
	else m_state = st;
}
RESULT Window::HandleEvent(const INPUT_RECORD events){
	m_tree->HandleEvent(events);
	m_cover->HandleEvent(events);
	return NONE;
}
void Window::SetActivity(STATE st)
{
	m_state = st;
	Draw(st);
}
STATE Window::GetActivity()
{
	return m_state;
}
Tree* Window::GetTree()
{
	return m_tree;
}
string Window::GetPathBySelector()
{
	return m_tree->GetPathBySelector();
}
Path* Window::GetPath()
{
	return m_tree->GetPath();
}