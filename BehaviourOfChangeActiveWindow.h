#ifndef BEHAVIOUROFCHANGEACTIVEWINDOW_H
#define BEHAVIOUROFCHANGEACTIVEWINDOW_H


#include "Command.h"
#include "TotalCommander.h"

//��������� ������ �� ���������
class BehaviourOfChangeActiveWindow : public Command
{
	TotalCommander* m_tc;
	Window* m_lwnd;
	Window* m_rwnd;
public:
	BehaviourOfChangeActiveWindow();
	explicit BehaviourOfChangeActiveWindow(TotalCommander* tc);
	virtual bool Do();
};

#endif /* BEHAVIOUROFCHANGEACTIVEWINDOW_H */
