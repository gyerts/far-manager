#ifndef BEHAVIOUROFREAD_H
#define BEHAVIOUROFREAD_H

#include "Command.h"
#include "TotalCommander.h"

//��������� ������ �� ���������
class BehaviourOfRead : public Command
{
	TotalCommander* m_tc;
	Tree* m_tree;
public:
	BehaviourOfRead();
	explicit BehaviourOfRead(TotalCommander* tc);
	virtual bool Do();
};

#endif /* BEHAVIOUROFREAD_H */
