#include "Tree.h"
#include <iostream>
#include "ConsoleLib.h"
#include "Coord.h"
using namespace std;
#include <windows.h>
#include <io.h>
#include <stdlib.h>
#include <string.h>
#include "TYPE_OF_ELEMENT.h"

Tree::Tree(Coord coord, STATE st, int wi, Path* pth) 
	: m_coord(coord.MoveDown().MoveRight()), 
	m_selector(0),
	m_state(st),
	m_from(0),
	m_width(wi),
	m_path(pth),
	m_elJustDeleted(false),
	m_makeDir(false)
{
	Cls();
	ClearPage();
	GoPath(*m_path);
}

void		Tree::Draw(STATE st)
{
	m_path->Draw(st);

	if(m_elJustDeleted) 
	{
		if(m_from && (m_page_names.size() - m_from + 1 == QUANTITY_ELEM_OF_TREE_ON_PAGE))
		{
			m_from--;
		}
	}

	setlocale(0, "");
	static int i, size, nameSize;

	switch(st)
	{
	case REDRAW:
		st = ACTIVATED;

	case DISACTIVATED:
	case ACTIVATED:
		if(m_state == st || m_state == DISABLED || st == REDRAW)
		{
			if(m_page_types[0 + m_from] == RETURN_tree)
			{
				GotoXY(m_coord);
				cout << m_page_names[0];
				m_coord.GoToNextLine();
				i = 1;
			}
			else i = 0;

			int temp;

			if(m_makeDir)
				temp = QUANTITY_ELEM_OF_TREE_ON_PAGE - 1;
			else
				temp = QUANTITY_ELEM_OF_TREE_ON_PAGE;

			for(size = m_page_names.size(); (i < size) && (i < temp); ++i)
			{
				//���� ������� ����������
				if(m_page_selection[i + m_from])
				{
					SetColor(LightRed, ConsoleColor(COLOR::BG_OF_WINDOW));
				}
				else
				{
					SetColor((COLOR::COLOR)m_page_types[i + m_from], COLOR::BG_OF_WINDOW);
				}
				//������� ���
				GotoXY(m_coord);

				//���� �� ����� �� ������ ��� ��������� �����, �� ���������� ��� ����� (��������� ��� ������)
				if(m_makeDir)
				{
					if(i == m_selector)
					{
						m_coord.GoToNextLine();
						ChangeTextAttr(m_coord.GetY(), m_coord.GetX(), COLOR::BG_OF_WINDOW, COLOR::BG_OF_WINDOW, m_width - 2);
						m_makeDir = false;
						m_coord.GoToNextLine();
						continue;
					}
				}

				if(m_page_names[i + m_from].size() < LENTH_TREE_ELEMENT_NAME - 4)
				{
					cout << m_page_names[i + m_from];
					nameSize = m_page_names[i + m_from].size() > LENTH_TREE_ELEMENT_NAME?LENTH_TREE_ELEMENT_NAME:m_page_names[i + m_from].size();
					GotoXY(m_coord);
					ChangeTextAttr(m_coord.GetY() + nameSize, i + 1, COLOR::BG_OF_WINDOW, COLOR::BG_OF_WINDOW, LENTH_TREE_ELEMENT_NAME - nameSize);
				}
				else
				{
					for(int j = 0; j < LENTH_TREE_ELEMENT_NAME - 4; ++j)
					{
						cout << m_page_names[i + m_from][j];
					}
					SetColor(COLOR::COLOR(Red), COLOR::BG_OF_WINDOW);
					cout << " >> ";
					SetColor((COLOR::COLOR)m_page_types[i + m_from], COLOR::BG_OF_WINDOW);
				}

				//������� ���
				GotoXY(m_coord.MoveRight(LENTH_TREE_ELEMENT_NAME));//----------->
				cout << GetStrByType(m_page_types[i + m_from]);
				ChangeTextAttr(m_coord.GetY(), i + 1, COLOR::COLOR(m_page_types[i + m_from]), COLOR::BG_OF_WINDOW, LENTH_TREE_ELEMENT_TYPE);

				//������� �������
				GotoXY(m_coord.MoveRight(LENTH_TREE_ELEMENT_TYPE));//----------->
				SetColor(COLOR::COLOR(m_page_types[i + m_from]), COLOR::BG_OF_WINDOW);
				if(m_page_types[i + m_from] == FOLDER_tree) //���� ����� �� ������ �� ������ 
				{
					ChangeTextAttr(m_coord.GetY(), i + 1, COLOR::BG_OF_WINDOW, COLOR::BG_OF_WINDOW, LENTH_TREE_ELEMENT_SIZE);
				}
				else if(m_page_sizes[i + m_from] / 1024 < 1) 
				{
					cout.width(LENTH_TREE_ELEMENT_SIZE/ 2);
					cout << m_page_sizes[i + m_from] << " b"; 
				}
				else if(m_page_sizes[i + m_from] / 1024 / 1024 < 1) 
				{
					cout.width(LENTH_TREE_ELEMENT_SIZE/ 2);
					cout << m_page_sizes[i + m_from] / 1024 << " kb"; 
				}
				else if(m_page_sizes[i + m_from] / 1024 / 1024 / 1024 < 1) 
				{
					cout.width(LENTH_TREE_ELEMENT_SIZE/ 2);
					cout << m_page_sizes[i + m_from] / 1024 / 1024 << " mb"; 
				}
				else if(m_page_sizes[i + m_from] / 1024 / 1024 / 1024 / 1024 < 1) 
				{
					cout.width(LENTH_TREE_ELEMENT_SIZE/ 2);
					cout << m_page_sizes[i + m_from] / 1024 / 1024 / 1024 << " gb"; 
				}

				if(i == 0 && m_selector == 0)
				{
					m_coord.MoveLeft(LENTH_TREE_ELEMENT_NAME).MoveLeft(LENTH_TREE_ELEMENT_TYPE);
					ChangeTextAttr(m_coord.GetY(), i + 1, (COLOR::COLOR)m_page_types[i + m_from], COLOR::BG_OF_SELECTED_EL_IN_TREE, nameSize);
					m_coord.MoveRight(nameSize);
					ChangeTextAttr(m_coord.GetY(), i + 1, COLOR::BG_OF_SELECTED_EL_IN_TREE, COLOR::BG_OF_SELECTED_EL_IN_TREE, LENTH_TREE_ELEMENT_NAME - nameSize);
					m_coord.MoveRight(LENTH_TREE_ELEMENT_NAME - nameSize);
					ChangeTextAttr(m_coord.GetY(), i + 1, (COLOR::COLOR)m_page_types[i + m_from], COLOR::BG_OF_SELECTED_EL_IN_TREE, LENTH_TREE_ELEMENT_SIZE + LENTH_TREE_ELEMENT_TYPE);
				}
				m_coord.GoToNextLine();
			}
			m_coord.GoToBurnPosition();
		}
		if(st == ACTIVATED || st == DISACTIVATED)
		{
			ChangeTextAttr(m_coord.GetY(), m_coord.GetX() + m_selector, COLOR::BG_BACKGOUND, COLOR::BG_BACKGOUND); 

			if(m_elJustDeleted) 
			{
				if(m_selector == m_page_names.size())
				{
					ChangeTextAttr(m_coord.GetY(), m_coord.GetX() + m_selector, COLOR::BG_OF_WINDOW, COLOR::BG_OF_WINDOW, m_width - 2);
				}
				else
				{
					ChangeTextAttr(m_coord.GetY(), m_coord.GetX() + m_page_names.size(), COLOR::BG_OF_WINDOW, COLOR::BG_OF_WINDOW, m_width - 2);
				}
				m_selector = 0;
				m_elJustDeleted = false;
			}
			if(!m_page_selection[m_selector + m_from])
			{
				ChangeTextAttr(m_coord, m_selector, m_from, m_page_types, st == ACTIVATED, m_page_names);
			}
		}
	}
	m_state = st;
}
void		Tree::GoPath(const Path& path)
{
	char fileMask[_MAX_PATH] = {};
	m_path->CopyToStr(fileMask);

	strcat(fileMask, "/*.*");

	_finddata_t finddata;
	long handle;

	// �������� �����. ���� ������ �� ����� - �������.


	if(m_path->GetPrevPathSize() > 0)
	{
		m_page_names.push_back("<-------------");
		m_page_types.push_back(RETURN_tree);
		m_page_sizes.push_back(0);
		m_page_selection.push_back(false);
	}

	//���� ��������
	if ((handle = _findfirst(fileMask, &finddata)) == -1) return;
	do{
		// ���� ����� �������
		if (finddata.attrib & _A_SUBDIR &&
			strcmp(finddata.name, ".") &&
			strcmp(finddata.name, ".."))
		{
			m_page_names.push_back(finddata.name);
			m_page_types.push_back(FOLDER_tree);
			m_page_sizes.push_back(finddata.size);
			m_page_selection.push_back(false);
		}
	}
	while (_findnext(handle, &finddata) != -1);

	//���� �����
	if ((handle = _findfirst(fileMask, &finddata)) == -1) return;
	do{
		if (!(finddata.attrib & _A_SUBDIR))
		{
			m_page_names.push_back(finddata.name);
			m_page_types.push_back(FILE_tree);
			m_page_sizes.push_back(finddata.size);
			m_page_selection.push_back(false);
		}
	}
	while (_findnext(handle, &finddata) != -1);
	_findclose(handle);
}
RESULT		Tree::HandleEvent(const INPUT_RECORD events)
{
	setlocale(0, "");

	static DWORD mouseButtonState;
	static COORD mousePos;

	switch(events.EventType)
	{
	case MOUSE_EVENT:
		mouseButtonState = events.Event.MouseEvent.dwButtonState;
		mousePos = events.Event.MouseEvent.dwMousePosition;

		if (events.Event.MouseEvent.dwEventFlags == MOUSE_MOVED)
		{
			//���� �� ��������� � ���� �� ���������
			if(((mousePos.Y >= m_coord.GetX()) && (mousePos.Y < m_page_names.size() + m_coord.GetX()))
				//� ���� �� ��������� � ���� �� �����������
					&& ((mousePos.X >= m_coord.GetY()) && (mousePos.X < m_width - 2 + m_coord.GetY())))
			{
				if(mousePos.Y < QUANTITY_ELEM_OF_TREE_ON_PAGE + m_coord.GetX())
				{

					if(!m_page_selection[m_selector + m_from])
						ChangeTextAttr(m_coord, m_selector, m_from, m_page_types, false, m_page_names);

					m_selector = mousePos.Y - m_coord.GetX();

					if(!m_page_selection[m_selector + m_from])
						ChangeTextAttr(m_coord, m_selector, m_from, m_page_types, true, m_page_names);
				}
			}
		}

		// ������ ����� ������ ����
		if (mouseButtonState == FROM_LEFT_1ST_BUTTON_PRESSED)
		{
			// ������� ��������� (Shift, Ctrl, Alt, etc.)
			DWORD ControlKeyState = events.Event.MouseEvent.dwControlKeyState;

			// ������ ������������ Shift, Ctrl � Alt
			if (ControlKeyState & SHIFT_PRESSED &&
				(ControlKeyState & LEFT_CTRL_PRESSED || ControlKeyState & RIGHT_CTRL_PRESSED) &&
				(ControlKeyState & LEFT_ALT_PRESSED || ControlKeyState & RIGHT_ALT_PRESSED));
			// ������ ����� Shift
			else if (ControlKeyState & SHIFT_PRESSED);
			// ����� ����� Ctrl
			else if (ControlKeyState & LEFT_CTRL_PRESSED || ControlKeyState & RIGHT_CTRL_PRESSED);
			// ����� ����� Alt
			else if (ControlKeyState & LEFT_ALT_PRESSED || ControlKeyState & RIGHT_ALT_PRESSED);
			// ������ ���� ���� ��� ������� ������ ���������
			else
			{

			}
		}

		if(mouseButtonState == RIGHTMOST_BUTTON_PRESSED)
		{

		}
		break;

	case KEY_EVENT:
		if (events.Event.KeyEvent.bKeyDown)
		{
			switch(events.Event.KeyEvent.wVirtualKeyCode)
			{
			case VK_DOWN:	//DOWN ARROW
				MoveSelector(WAY::DOWN);
				break;
			case VK_UP:		//UP ARROW
				MoveSelector(WAY::UP);
				break;
			case VK_RETURN:	//ENTER
				if(m_selector == 0 && m_path->GetPrevPathSize() > 0)
				{
					BackUp();
					Draw(m_state);
				}
				else
				{
					GoInside();
				}
				break;
			case VK_BACK:	//BACKSPACE
				BackUp();
				Draw(m_state);
				break;
			}
		}
	}
	return NONE;
}
void		Tree::BackUp()
{
	if(m_path->GetPrevPathSize() == 0)
	{
		return;
	}

	m_from = m_lastPositionOfFrom[m_lastPositionOfFrom.size() - 1];
	m_selector = m_lastPositionOfSelector[m_lastPositionOfSelector.size() - 1];

	m_lastPositionOfSelector.pop_back();
	m_lastPositionOfFrom.pop_back();

	m_path->SetPath(m_path->GetPrevPath());

	int counter = 0;
	int size = m_path->GetSize();

	int position = 0;

	while(size)
	{
		if((*m_path)[size] == '/') 
		{
			position = size;
			break;
		}
		--size;
	}
	//m_prevPath.clear();
	m_path->ClearPrevPath();


	size = m_path->GetSize();
	while(size)
	{
		if((*m_path)[size] == '/')
			counter++;
		--size;
	}
	for(int i = 0; (i < position) && (counter > 0); i++)
	{
		m_path->PrevPathPushChar((*m_path)[i]);
	}
	Cls();
	ClearPage();
	GoPath(*m_path);
}
void		Tree::SetPathBySelector()
{
	m_path->SetPrevPath(m_path->GetPath());
	//��������� ���� ������ ������������� ��������
	if(m_page_names.size() > m_selector)
	{
		*m_path += '/';
		*m_path += m_page_names[m_selector + m_from];
	}
}
void		Tree::ClearPage()
{
	m_page_names.clear();
	m_page_sizes.clear();
	m_page_types.clear();
	m_page_selection.clear();
}
void		Tree::MoveSelector(WAY::WAY way)
{
	switch(way)
	{
	case WAY::DOWN:
		if(m_selector < m_page_names.size() - 1)
		{
			if(m_selector == QUANTITY_ELEM_OF_TREE_ON_PAGE - 1)
			{
				if(m_page_names.size() > QUANTITY_ELEM_OF_TREE_ON_PAGE + m_from)
				{
					++m_from;
					Draw(m_state);
				}
				return;
			}

			if(!m_page_selection[m_selector + m_from])
				ChangeTextAttr(m_coord, m_selector, m_from, m_page_types, false, m_page_names);
			++m_selector;
			if(!m_page_selection[m_selector + m_from])
				ChangeTextAttr(m_coord, m_selector, m_from, m_page_types, true, m_page_names);
		}
		break;

	case WAY::UP:
		if(m_selector > 0 || m_from > 0)
		{
			if(m_selector == 0)
			{
				if(m_from > 0)
				{
					--m_from;
					Draw(m_state);
				}
				return;
			}

			if((m_selector + m_from) > 0 && !m_page_selection[m_selector + m_from])
				ChangeTextAttr(m_coord, m_selector, m_from, m_page_types, false, m_page_names);
			--m_selector;
			if(!m_page_selection[m_selector + m_from])
				ChangeTextAttr(m_coord, m_selector, m_from, m_page_types, true, m_page_names);
		}
		break;
	}
}
void		Tree::SetActivity(STATE st)
{
	m_state = st;
}
void		Tree::GoInside()
{
	if(m_page_types[m_from + m_selector] == FOLDER_tree)
	{
		m_lastPositionOfSelector.push_back(m_selector);
		m_lastPositionOfFrom.push_back(m_from);
		SetPathBySelector();
		m_selector = 0;
		m_from = 0;

		Cls();
		ClearPage();
		GoPath(*m_path);
		Draw(m_state);
	}
	else
	{
		//ShellExecute
	}
}
bool		Tree::GoBack()
{
	if(m_selector == 0 && m_path->GetPrevPathSize() > 0 && m_from == 0)
	{
		BackUp();
		Draw(m_state);
		return true;
	}
	return false;
}
bool		Tree::AreYouHere(COORD mousePos)
{
	if(((mousePos.Y >= m_coord.GetX()) && (mousePos.Y < GetPageSize() + m_coord.GetX()))
		&& ((mousePos.X >= m_coord.GetY()) && (mousePos.X < LENTH_TREE_ELEMENT_NAME + LENTH_TREE_ELEMENT_SIZE * 2 + m_coord.GetY())))
	{
		return true;
	}
	else return false;
}
void		Tree::Cls()
{
	m_coord.GoToBurnPosition();

	for(int i = 0, size = (m_page_names.size()>QUANTITY_ELEM_OF_TREE_ON_PAGE?
		QUANTITY_ELEM_OF_TREE_ON_PAGE:m_page_names.size()); i < size; ++i)
	{
		GotoXY(m_coord);

		ChangeTextAttr(m_coord.GetY(), m_coord.GetX(), 
			COLOR::BG_OF_WINDOW,
			COLOR::BG_OF_WINDOW,
			m_width - 2);

		m_coord.GoToNextLine();
	}


	SetColor(COLOR::BG_OF_WINDOW, COLOR::BG_OF_WINDOW);
	static char* str = nullptr;

	if(!str)
	{
		str = new char[m_width - 1];
		for(int i = 0; i < m_width - 2; ++i)
		{
			str[i] = ' ';
		}
		str[m_width - 2] = NULL;
	}



	m_coord.GoToBurnPosition();
	for(int i = 0, size = (m_page_names.size()>QUANTITY_ELEM_OF_TREE_ON_PAGE?
		QUANTITY_ELEM_OF_TREE_ON_PAGE:m_page_names.size()); i < size; ++i)
	{
		GotoXY(m_coord);
		cout << str;
		m_coord.GoToNextLine();
	}

	m_coord.GoToBurnPosition();

}
void		Tree::IJustDeleteElement()
{
	m_elJustDeleted = true;
}
void		Tree::ICreateFolder()
{
	m_makeDir = true;
}
void		Tree::SelectSelectedItem()
{
	if(m_page_types[m_selector + m_from] != RETURN_tree)
		m_page_selection[m_selector + m_from] = !m_page_selection[m_selector + m_from];
}
int			Tree::GetSelector() const
{
	return m_selector;
}
Path*		Tree::GetPath()
{
	return m_path;
}
Coord		Tree::GetCoord() const
{
	return m_coord;
}
const char* Tree::GetStrByType(TYPE_OF_ELEMENT_TREE tp) const
{
	if(tp == FILE_tree)
	{
		static char files[LENTH_TREE_ELEMENT_TYPE + 2] = "< ����  >    ";
		return files;
	}
	else if(tp == FOLDER_tree)
	{
		static char folders[LENTH_TREE_ELEMENT_TYPE + 2] = "< ����� >    ";
		return folders;
	}
	else if(tp == ZIP_tree)
	{
		static char zips[LENTH_TREE_ELEMENT_TYPE + 2] = "< ����� >    ";
		return zips;
	}
	else if(tp == RAR_tree)
	{
		static char rars[LENTH_TREE_ELEMENT_TYPE + 2] = "< ����� >    ";
		return rars;
	}
	else
	{
		static char returns[1] = {NULL};
		return returns;
	}
}
string		Tree::GetPathBySelector()
{
	string str = m_path->GetPath();

	if(m_page_names.size() > m_selector)
	{
		if(!(m_selector + m_from))
		{
			str.clear();
		}
		else
		{
			str += '/';
			str += m_page_names[m_selector + m_from];
		}
	}

	return str;
}
Coord		Tree::GetCoord()
{
	return m_coord;
}
int			Tree::GetPageSize()
{
	return m_page_names.size();
}
int			Tree::GetWidth() const
{
	return m_width - 2;
}
vector<pair<string, TYPE_OF_ELEMENT_TREE> >*	Tree::GetVectorOfSelectedElements()
{
	vector<pair<string, TYPE_OF_ELEMENT_TREE> >* ar = new vector<pair<string, TYPE_OF_ELEMENT_TREE> >;

	for(int i = 0, size = m_page_selection.size(); i < size; ++i)
	{
		if(m_page_selection[i]) ar->push_back(pair<string, TYPE_OF_ELEMENT_TREE>(m_page_names[i], m_page_types[i]));
	}

	return ar;
}