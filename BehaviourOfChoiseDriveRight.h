#ifndef BEHAVIOUROFCHOISEDRIVERIGHT_H
#define BEHAVIOUROFCHOISEDRIVERIGHT_H


#include "Command.h"
#include "TotalCommander.h"

//��������� ������ �� ���������
class BehaviourOfChoiseDriveRight : public Command
{
	TotalCommander* m_tc;
	Window* m_lwnd;
	Window* m_rwnd;
public:
	BehaviourOfChoiseDriveRight();
	explicit BehaviourOfChoiseDriveRight(TotalCommander* tc);
	virtual bool Do();
};
#endif /* BEHAVIOUROFCHOISEDRIVERIGHT_H */
