#ifndef PATH_H
#define PATH_H


#include "Coord.h"
#include <string>
#include <cstring>
#include <iostream>
#include "ConsoleLib.h"
#include "Option.h"
#include "IDrawable.h"

using std::cout;

class Path : public IDrawable
{
	string		m_path;
	Coord		m_coord;
	STATE		m_state;
	int			m_width;
	string		m_prevPath;
public:
	Path(Coord crd, int w, STATE st, char path) : m_state(st), m_coord(crd), m_width(w){
		m_path += path;
		m_path += ':';
	}
	Path& operator +=(const char* str)
	{
		m_path += str;
		return *this;
	}
	Path& operator +=(const char ch)
	{
		m_path += ch;
		return *this;
	}
	Path& operator +=(const string& str)
	{
		m_path += str;
		return *this;
	}
	void Draw(STATE st)
	{
		m_coord.GoToBurnPosition();
		static COLOR::COLOR cl;
		switch(st)
		{
		case ACTIVATED:
		case DISACTIVATED:
		case REDRAW:
			cl = st == ACTIVATED?COLOR::BG_PATH:COLOR::BG_OF_WINDOW;

			if(m_state != DISABLED)
			{
				m_coord.GoToBurnPosition();
				SetColor(COLOR::TXT_ACTIVE, COLOR::BG_OF_WINDOW);
				GotoXY(m_coord.MoveRight());
				setlocale(LC_CTYPE,"C");
				for(int i = 0; i < m_width - 2; ++i)
				{
					cout << char(205);
				}
			}
			SetColor(COLOR::TXT_ACTIVE, cl);
			if(m_path.size() > unsigned(m_width-2))
			{
				GotoXY(m_coord);
				bool dbl = false;
				for(int i = m_path.size() - m_width + 4, size = m_path.size(); i < size; ++i)
				{
					if(!dbl) 
					{
						cout << "..";
						dbl = true;
					}
					cout << m_path[i];
				}
			}
			else
			{
				GotoXY(m_coord.GoCenter(m_path, m_width));
				cout << m_path;
			}
			break;
		}
		if(st == REDRAW) m_state = ACTIVATED;
		else m_state = st;
		SetColor(COLOR::TXT_ACTIVE, COLOR::BG_OF_WINDOW);
	}
	int GetPrevPathSize()
	{
		return m_prevPath.size();
	}
	int GetSize()
	{
		return m_path.size();
	}
	char operator[](int shift)
	{
		return m_path[shift];
	}
	void CopyToStr(char* str)
	{
		for(int i = 0, size = m_path.size(); (i < _MAX_PATH) && (i < size + 1); ++i)
		{
			str[i] = m_path[i];
		}
	}

	string GetPrevPath()
	{
		return m_prevPath;
	}
	string GetPath()
	{
		return m_path;
	}
	void SetPrevPath(const string& path)
	{
		m_prevPath.clear();
		m_prevPath = path;
	}
	void SetPath(const string& path)
	{
		m_path.clear();
		m_path = path;
	}
	void ClearPrevPath()
	{
		m_prevPath.clear();
	}
	void PrevPathPushChar(char ch)
	{
		m_prevPath += ch;
	}
};
#endif /* PATH_H */
