#ifndef BEHAVIOUROFPASTE_H
#define BEHAVIOUROFPASTE_H

#include "Command.h"
#include "TotalCommander.h"

//��������� ������ �� ���������
class BehaviourOfPaste : public Command
{
	TotalCommander* m_tc;
public:
	BehaviourOfPaste();
	explicit BehaviourOfPaste(TotalCommander* tc);
	virtual bool Do();
};

#endif /* BEHAVIOUROFPASTE_H */
