#include "BehaviourOfCopy.h"
#include <iostream>

#include <string>
#include <cstring>

using std::cout;
using std::endl;
using std::wstring;

BehaviourOfCopy::BehaviourOfCopy(TotalCommander* tc):
	m_tc(tc){}
bool BehaviourOfCopy::Do()
{
	m_tc->CleanBuffer();
	Tree* tree = m_tc->GetActiveWindow()->GetTree();
	m_tc->SetBuffer(tree->GetVectorOfSelectedElements());
	m_tc->SetBufferPuth(tree->GetPath()->GetPath());
	return true;
}