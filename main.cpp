#include <stdio.h>

#include "TotalCommander.h"
#include "HeaderWithAllCommands.h"
#include "Path.h"
#include "LENTH.h"
#include "CONSOLE_COLOR.h"
#include "PROGRAM_COLOR.h"
#include "Window.h"

TotalCommander* TotalCommander::m_instance = TotalCommander::GetInstance();


int main(int args, char** argv)
{
	char str[256] = {};
	printf(str, "mode con cols=%d lines=%d", WINDOW_WIDTH, WINDOW_HEIGHT);
	system (str);

	SetColor(COLOR::BG_OF_WINDOW, COLOR::BG_OF_WINDOW);
	system("cls");

	Coord* crd = new Coord(0, 0);
	Path* path = new Path(*crd, WINDOW_WIDTH / 2, DISABLED, 'C');
	Tree* tree = new Tree(*crd, DISABLED, WINDOW_WIDTH / 2, path);

	Decorator* decorator = new BorderOfFrame(*crd, WINDOW_WIDTH / 2, WINDOW_HEIGHT - 4, new Background(*crd, WINDOW_WIDTH / 2, WINDOW_HEIGHT - 4, DISABLED, COLOR::BG_OF_WINDOW), DISABLED, COLOR::BG_OF_WINDOW);

	Window* left = new Window(*crd, WINDOW_WIDTH / 2, WINDOW_HEIGHT - 4, DISABLED, tree, decorator, path);

	crd = new Coord(0, WINDOW_WIDTH / 2);

	path = new Path(*crd, WINDOW_WIDTH / 2, DISABLED, 'C');
	tree = new Tree(*crd, DISABLED, WINDOW_WIDTH / 2, path);
	decorator = new BorderOfFrame(*crd, WINDOW_WIDTH / 2, WINDOW_HEIGHT -4, new Background(*crd, WINDOW_WIDTH / 2, WINDOW_HEIGHT - 4, DISABLED, COLOR::BG_OF_WINDOW), DISABLED, COLOR::BG_OF_WINDOW);

	Window* right = new Window(*crd, WINDOW_WIDTH / 2, WINDOW_HEIGHT - 4, DISABLED, tree, decorator, path);

	TotalCommander* tc = TotalCommander::GetInstance();

	tc->SetWindows(left, right);

	tc->AddCommand(new BehaviourOfCopy(tc), COPY);
	/*tc->AddCommand(new BehaviourOfPaste(tc), PASTE);*/
	tc->AddCommand(new BehaviourOfMove(tc), MOVE);
	tc->AddCommand(new BehaviourOfRead(tc), OPEN_FOLDER);
	tc->AddCommand(new BehaviourOfRead(tc), READ);
	/*tc->AddCommand(new BehaviourOfMakeDir(tc), MAKE_DIR);*/
	/*tc->AddCommand(new BehaviourOfDelete(tc), DELETE_ITEM);*/
	tc->AddCommand(new BehaviourOfChangeActiveWindow(tc), GO_TO_OTHER_WINDOW);
	tc->AddCommand(new BehaviourOfExit(tc), EXIT);
	tc->AddCommand(new BehaviourOfCloseLeft(tc), CLOSE_LEFT);
	tc->AddCommand(new BehaviourOfCloseRight(tc), CLOSE_RIGHT);
	tc->AddCommand(new BehaviourOfChoiseDriveLeft(tc), CHS_DRV_LEFT);
	tc->AddCommand(new BehaviourOfChoiseDriveRight(tc), CHS_DRV_RIGHT);

	tc->LetsGetUsersInput();
	return 0;
}