#ifndef CONSOLELIB_H
#define CONSOLELIB_H

#include <windows.h>
#include "Option.h"
#include "Coord.h"
#include <vector>
// ������ �������
extern HANDLE hStdOut;
extern HANDLE hStdIn;



// ����������/������ ��������� ������
void ShowCursor(bool visible);

// ������������� ���� �������� � ����
void SetColor(ConsoleColor text, ConsoleColor background);
void SetColor(COLOR::COLOR text, COLOR::COLOR background);
void SetColor(ConsoleColor text);

// ���������� ������ � �������� �������
void GotoXY(Coord&);

// ������� �������� ������ � �������� �������
void WriteStr(int X, int Y, const char *Str);

// ������� �������� ������ ������� � �������� �������
void WriteChar(int X, int Y, char Ch);

// ������� ��������� ���������� �������� �������� ������� � �������� �������
void WriteChars(int X, int Y, char Ch, unsigned Len);

// ������ ��������� ���������, ������� � �������� �������
void ChangeTextAttr(int X, int Y, ConsoleColor text, ConsoleColor background, unsigned Len);
void ChangeTextAttr(int X, int Y, COLOR::COLOR text, COLOR::COLOR background, unsigned Len);
void ChangeTextAttr(int X, int Y, COLOR::COLOR background, unsigned Len);
void ChangeTextAttr(Coord crd, int selector, int from, std::vector<TYPE_OF_ELEMENT_TREE>& types, bool selected, std::vector<std::string>& names);


#endif /* CONSOLELIB_H */
