#ifndef BEHAVIOUROFDELETE_H
#define BEHAVIOUROFDELETE_H

#include "Command.h"
#include "TotalCommander.h"
#include "menu.h"

//��������� ������ �� ���������
class BehaviourOfDelete : public Command
{
	TotalCommander* m_tc;
	Menu*			m_menu;

public:
	BehaviourOfDelete();
	explicit BehaviourOfDelete(TotalCommander* tc);
	virtual bool Do();
};

#endif /* BEHAVIOUROFDELETE_H */
