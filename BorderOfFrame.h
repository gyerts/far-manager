#ifndef BORDEROFFRAME_H
#define BORDEROFFRAME_H


#include "Coord.h"
#include "Decorator.h"
#include "Widget.h"
#include "Observer.h"

class BorderOfFrame: public Decorator, public Observer
{
	//���������� Background
	Coord						m_coord;
	//������ � ������ Background
	int							m_width;
	int							m_height;

	STATE						m_state;
	COLOR::COLOR				m_color;

public:
	BorderOfFrame (Coord coord, int width, int height, Widget* w, STATE st, COLOR::COLOR);
	virtual void Draw(STATE st);
	virtual RESULT HandleEvent(const INPUT_RECORD events);
	virtual void SetActivity(STATE st);
};
#endif /* BORDEROFFRAME_H */
