#include "BehaviourOfRead.h"
#include "LENTH.h"
#include <iostream>

using std::cout;
using std::endl;

BehaviourOfRead::BehaviourOfRead(TotalCommander* tc):
	m_tc(tc){}
bool BehaviourOfRead::Do()
{
	m_tree = m_tc->GetActiveWindow()->GetTree();
	static COORD mousePos = m_tc->GetMouseCoord();
	static Coord m_coord = m_tree->GetCoord();

	if(!m_tree->GoBack())
	{
		if(((mousePos.Y >= m_coord.GetX()) && (mousePos.Y < m_tree->GetPageSize() + m_coord.GetX()))
			&& ((mousePos.X >= m_coord.GetY()) && (mousePos.X < LENTH_TREE_ELEMENT_NAME + m_coord.GetY())))
		{
			m_tree->GoInside();
		}
	}
	return true;
}