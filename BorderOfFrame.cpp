#include "BorderOfFrame.h"
#include "ConsoleLib.h"

#include <iostream>
using std::cout;

BorderOfFrame::BorderOfFrame (Coord coord, int width, int height, Widget* w, STATE st, COLOR::COLOR cl) 
	: Decorator(w),
	m_coord(coord),
	m_width(width),
	m_height(height),
	m_state(st),
	m_color(cl){}
void BorderOfFrame::Draw(STATE st)
{
	setlocale(LC_CTYPE,"C");
	Decorator::Draw(st);

	switch(st)
	{
	case DISABLED:
		break;

	case ACTIVATED:
	case DISACTIVATED:
	case REDRAW:
		if(((m_state == ACTIVATED || m_state == DISACTIVATED) && st != REDRAW) && m_state == DISABLED);
		else
		{
			int x = m_coord.GetX();
			int y = m_coord.GetY();

			SetColor(COLOR::TXT_ACTIVE, m_color);

			for(int i = 0; i < m_height; i++)
			{
				for(int j = 0; j < m_width; j++)
				{
					if(	i == 0 ||
						j == 0 ||
						i == m_height - 1 ||
						j == m_width - 1)
					{ 
						GotoXY(m_coord);

						//������� ���� ����� � ������
						if(j == 0 && i == 0) cout << char(201);
						else if(j == m_width - 1 && i == 0) cout << char(187);

						//������ ���� ����� � ������
						else if(j == 0 && i == m_height - 1) cout << char(200);
						else if(j == m_width - 1 && i == m_height - 1) cout << char(188);

						//������������ �����
						else if(j == 0 || j == m_width - 1) cout << char(186);

						//�������������� �����
						else if(i == 0 || i == m_height - 1) cout << char(205);
						m_coord.MoveRight();
					}
					else
					{
						m_coord.MoveRight();
					}
				}
				GotoXY(m_coord.GoToNextLine());
			}
			m_coord.GoToBurnPosition();
		}
		if(st == REDRAW) m_state = ACTIVATED;
		else m_state = st;
	}
}
RESULT BorderOfFrame::HandleEvent(const INPUT_RECORD events)
{
	Decorator::HandleEvent(events);

	setlocale(0, "");

	DWORD mouseButtonState;
	switch(events.EventType)
	{
	case MOUSE_EVENT:
		mouseButtonState = events.Event.MouseEvent.dwButtonState;
		switch(mouseButtonState)
		{
		case RIGHTMOST_BUTTON_PRESSED:
			GotoXY(m_coord);
			cout << "����� ���� �������� ��������� ������ ����";
			break;
		}
		break;
	}
	return NONE;
}
void BorderOfFrame::SetActivity(STATE st)
{
	Decorator::SetActivity(st);
	m_state = st;
}