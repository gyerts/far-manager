CC = g++
CPPFLAGS = -std=c++11

PROG = myapp
OBJS = main.o Background.o BehaviourOfChangeActiveWindow.o BehaviourOfChoiseDriveLeft.o BehaviourOfChoiseDriveRight.o BehaviourOfCloseLeft.o BehaviourOfCloseRight.o BehaviourOfCopy.o BehaviourOfExit.o BehaviourOfMove.o BehaviourOfRead.o BorderOfFrame.o ConsoleLib.o Coord.o Decorator.o LENTH.o TotalCommander.o Tree.o Window.o
H_FILES = Option.h Button.h Command.h COMMANDS.h CONSOLE_COLOR.h PROGRAM_COLOR.h Path.h Options.h Observer.h Observable.h menu.h IDrawable.h HeaderWithAllCommands.h ToolBar.h TYPE_OF_ELEMENT.h Widget.h WAY.h String.h STATE.h

$(PROG) : $(OBJS) 
	$(CC) $(CPPFLAGS) -o $(PROG) $(OBJS)

main.o : LENTH.h
	$(CC) $(CPPFLAGS) -c main.cpp $(H_FILES) 

Background.o : Background.h Background.cpp
	$(CC) $(CPPFLAGS) -c Background.cpp

BehaviourOfChangeActiveWindow.o : BehaviourOfChangeActiveWindow.h BehaviourOfChangeActiveWindow.cpp
	$(CC) $(CPPFLAGS) -c BehaviourOfChangeActiveWindow.cpp

BehaviourOfChoiseDriveLeft.o : BehaviourOfChoiseDriveLeft.h BehaviourOfChoiseDriveLeft.cpp
	$(CC) $(CPPFLAGS) -c BehaviourOfChoiseDriveLeft.cpp

BehaviourOfChoiseDriveRight.o : BehaviourOfChoiseDriveRight.h BehaviourOfChoiseDriveRight.cpp
	$(CC) $(CPPFLAGS) -c BehaviourOfChoiseDriveRight.cpp

BehaviourOfCloseLeft.o : BehaviourOfCloseLeft.h BehaviourOfCloseLeft.cpp
	$(CC) $(CPPFLAGS) -c BehaviourOfCloseLeft.cpp

BehaviourOfCloseRight.o : BehaviourOfCloseRight.h BehaviourOfCloseRight.cpp
	$(CC) $(CPPFLAGS) -c BehaviourOfCloseRight.cpp

BehaviourOfCopy.o : BehaviourOfCopy.h BehaviourOfCopy.cpp
	$(CC) $(CPPFLAGS) -c BehaviourOfCopy.cpp

BehaviourOfDelete.o : BehaviourOfDelete.h BehaviourOfDelete.cpp
	$(CC) $(CPPFLAGS) -c BehaviourOfDelete.cpp

BehaviourOfExit.o : BehaviourOfExit.h BehaviourOfExit.cpp
	$(CC) $(CPPFLAGS) -c BehaviourOfExit.cpp

# BehaviourOfMakeDir.o : BehaviourOfMakeDir.h BehaviourOfMakeDir.cpp
#  	$(CC) $(CPPFLAGS) -c BehaviourOfMakeDir.cpp

BehaviourOfMove.o : BehaviourOfMove.h BehaviourOfMove.cpp
	$(CC) $(CPPFLAGS) -c BehaviourOfMove.cpp

# BehaviourOfPaste.o : BehaviourOfPaste.h BehaviourOfPaste.cpp
# 	$(CC) $(CPPFLAGS) -c BehaviourOfPaste.cpp

BehaviourOfRead.o : BehaviourOfRead.h BehaviourOfRead.cpp
	$(CC) $(CPPFLAGS) -c BehaviourOfRead.cpp

BorderOfFrame.o : BorderOfFrame.h BorderOfFrame.cpp
	$(CC) $(CPPFLAGS) -c BorderOfFrame.cpp

ConsoleLib.o : ConsoleLib.h ConsoleLib.cpp
	$(CC) $(CPPFLAGS) -c ConsoleLib.cpp

Coord.o : Coord.h Coord.cpp
	$(CC) $(CPPFLAGS) -c Coord.cpp
 
Decorator.o : Decorator.h Decorator.cpp
	$(CC) $(CPPFLAGS) -c Decorator.cpp

LENTH.o : LENTH.h LENTH.cpp
	$(CC) $(CPPFLAGS) -c LENTH.cpp

TotalCommander.o : TotalCommander.h TotalCommander.cpp
	$(CC) $(CPPFLAGS) -c TotalCommander.cpp

Tree.o : Tree.h Tree.cpp
	$(CC) $(CPPFLAGS) -c Tree.cpp

Window.o : Window.h Window.cpp
	$(CC) $(CPPFLAGS) -c Window.cpp

clean:
	del *.o
	del *.gch