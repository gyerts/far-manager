/*switch(m_window[RIGHT]->GetActivity())
				{
				case ACTIVATED:
				case DISACTIVATED:
					m_window[RIGHT]->SetActivity(DISABLED);
					DeleteObserver(m_window[RIGHT]);
					if(m_window[LEFT]->GetActivity() == DISACTIVATED)
					{
						m_window[LEFT]->SetActivity(ACTIVATED);
						AddObserver(m_window[LEFT]);
					}
					break;
				case DISABLED:
					if(m_window[LEFT]->GetActivity() == ACTIVATED)
						m_window[RIGHT]->SetActivity(DISACTIVATED);
					else
					{
						m_window[RIGHT]->SetActivity(ACTIVATED);
						AddObserver(m_window[RIGHT]);
					}
				}*/
#include "BehaviourOfCloseRight.h"

BehaviourOfCloseRight::BehaviourOfCloseRight(TotalCommander* tc):
	m_tc(tc),
	m_lwnd(m_tc->GetWindow(m_tc->LEFT)),
	m_rwnd(m_tc->GetWindow(m_tc->RIGHT)){}

bool BehaviourOfCloseRight::Do()
{
	switch(m_rwnd->GetActivity())
	{
	case ACTIVATED:
	case DISACTIVATED:
		m_rwnd->SetActivity(DISABLED);
		m_tc->DeleteObserver(m_rwnd);
		if(m_lwnd->GetActivity() == DISACTIVATED)
		{
			m_lwnd->SetActivity(ACTIVATED);
			m_tc->AddObserver(m_lwnd);
		}
		break;
	case DISABLED:
		if(m_lwnd->GetActivity() == ACTIVATED)
			m_rwnd->SetActivity(DISACTIVATED);
		else
		{
			m_rwnd->SetActivity(ACTIVATED);
			m_tc->AddObserver(m_rwnd);
		}
	}
	return true;
}