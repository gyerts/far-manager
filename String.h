#ifndef STRING_H
#define STRING_H

#include "Button.h"
#include "LENTH.h"
#include <vector>

using std::vector;

class String
{
	Coord			m_ownCoord;
	Coord			m_PrevBtnCoord;
	vector<Button*> m_buttons;
public:
	void Draw()
	{
		for(int i = 0, size = m_buttons.size(); i < size && i < 8; ++i)
		{
			m_buttons[i]->Draw();
		}
	}
	String(Coord crd): m_ownCoord(crd), m_PrevBtnCoord(crd){}
	void AddButton(string name, string info)
	{
		m_buttons.push_back(new Button(name, info, m_PrevBtnCoord.MoveRight().MoveRight(), WINDOW_WIDTH / 8));
		m_PrevBtnCoord.MoveRight(WINDOW_WIDTH / 8);
	}
};

#endif /* STRING_H */
