#ifndef PROGRAM_COLOR_H
#define PROGRAM_COLOR_H

#include "CONSOLE_COLOR.h"

namespace COLOR
{
	enum COLOR{
		BG_OF_WINDOW =                Blue,
		BG_OF_MENU =                  Cyan,
		BG_OF_SELECTED_EL_IN_MENU =   Black,
		BG_OF_SELECTED_EL_IN_TREE =   Cyan,
		BG_BACKGOUND =                Black,
		BG_PATH =                     LightRed,
		BG_BUTTON =                   LightBlue,
		TXT_ACTIVE =                  White,
		TXT_NOACTIVE =                LightGray,

		TXT_OF_FILE_NAME =            Green,
		TXT_OF_FILE_TYPE =            Green,
		TXT_OF_FILE_SIZE =            Green,

		TXT_OF_FOLDER_NAME =          White,
		TXT_OF_FOLDER_TYPE =          White,
		TXT_OF_FOLDER_SIZE =          BG_OF_WINDOW
	};
};

#endif /* PROGRAM_COLOR_H */

