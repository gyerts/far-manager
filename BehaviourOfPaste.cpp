#include "BehaviourOfPaste.h"
#include <iostream>
#include "BehaviourOfMakeDir.h"
#include <string>
#include <cstring>

using std::wstring;
using std::cout;
using std::endl;

bool CreateDirectoryTree(LPCTSTR szPathTree, LPSECURITY_ATTRIBUTES lpSecurityAttributes = NULL);

bool CopyDirTo( const wstring& source_folder, const wstring& target_folder )
{
	wstring new_sf = source_folder + L"\\*";
	WCHAR sf[MAX_PATH + 1];
	WCHAR tf[MAX_PATH + 1];

	wcscpy_s(sf, MAX_PATH, new_sf.c_str());
	wcscpy_s(tf, MAX_PATH, target_folder.c_str());

	sf[lstrlen(sf) + 1] = 0;
	tf[lstrlen(tf) + 1] = 0;

	SHFILEOPSTRUCTW s = { 0 };
	s.wFunc = FO_COPY;
	s.pTo = tf;
	s.pFrom = sf;
	s.fFlags = FOF_SILENT | FOF_NOCONFIRMMKDIR | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_NO_UI;
	int res = SHFileOperation( &s );

	return res == 0;
}
bool CopyDirTo( const string& source_folder, const wchar_t* target_folder )
{
	wstring sf;
	wstring tf;

	for(int i = 0, size = source_folder.size(); i < size; ++i)
	{
		if(source_folder[i] == '/')
		{
			sf += (wchar_t)'\\';
		}
		else
		{
			sf += (wchar_t)source_folder[i];
		}
	}

	for(int i = 0, size = lstrlen(target_folder); i < size; ++i)
	{
		if(target_folder[i] == '/') 
		{
			tf += (wchar_t)'\\';
		}
		else
		{
			tf += target_folder[i];
		}
	}
	CopyDirTo(sf, tf);
	return true;
}

BehaviourOfPaste::BehaviourOfPaste(TotalCommander* tc):
	m_tc(tc){}
bool BehaviourOfPaste::Do()
{
	vector<pair<string, TYPE_OF_ELEMENT_TREE> >* buffer = m_tc->GetBuffer();
	Tree* tree = m_tc->GetActiveWindow()->GetTree();
	string path = tree->GetPath()->GetPath();

	for(int i = 0, size = buffer->size(); i < size; ++i)
	{
		wchar_t fin[_MAX_PATH] = {};

		int index = 0;//������� ������� ���
		int index2 = 0;

		int sizef = path.size();//����� ����� ����

		while(index < sizef)
		{
			char c = path[index];
			fin[index++] = c;
		}
		fin[index++] = '/';
		//������� ����� �����


		int limit = (*buffer)[i].first.size();
		//������ ������
		while(index < sizef + 1 + limit)
		{
			fin[index++] = (*buffer)[i].first[index2++];
		}
		CreateDirectoryTree(fin);
		//����� ���� ���������� �������


		//�������� ��� � ��� �����
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		string fromPath(m_tc->GetBufferPuth());
		fromPath += '/';
		fromPath += (*buffer)[i].first;

		CopyDirTo(fromPath, fin);
	}

	tree->ClearPage();
	tree->GoPath(*(tree->GetPath()));
	tree->Draw(ACTIVATED);

	return true;
}