#ifndef BEHAVIOUROFEXIT_H
#define BEHAVIOUROFEXIT_H


#include "Command.h"
#include "TotalCommander.h"

//��������� ������ �� ���������
class BehaviourOfExit : public Command
{
	TotalCommander* m_tc;
public:
	explicit BehaviourOfExit(TotalCommander* tc);
	virtual bool Do();
};
#endif /* BEHAVIOUROFEXIT_H */
