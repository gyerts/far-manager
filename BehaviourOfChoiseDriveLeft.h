#ifndef BEHAVIOUROFCHOISEDRIVELEFT_H
#define BEHAVIOUROFCHOISEDRIVELEFT_H


#include "Command.h"
#include "TotalCommander.h"

//��������� ������ �� ���������
class BehaviourOfChoiseDriveLeft : public Command
{
	TotalCommander* m_tc;
	Window* m_lwnd;
	Window* m_rwnd;
public:
	BehaviourOfChoiseDriveLeft();
	explicit BehaviourOfChoiseDriveLeft(TotalCommander* tc);
	virtual bool Do();
};


#endif /* BEHAVIOUROFCHOISEDRIVELEFT_H */
