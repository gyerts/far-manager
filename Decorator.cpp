#include "Decorator.h"

Decorator::Decorator(Widget *w) : wid(w) {}
Decorator::~Decorator() { delete wid; }
void Decorator::Draw(STATE st)
{
	wid->Draw(st);
}
RESULT Decorator::HandleEvent(const INPUT_RECORD events)
{
	wid->HandleEvent(events);
	return NONE;
}
void Decorator::SetActivity(STATE st)
{
	wid->SetActivity(st);
}