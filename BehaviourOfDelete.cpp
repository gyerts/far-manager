#include "BehaviourOfDelete.h"

#include <string>
#include <iostream>
#include <string>
#include <cstring>

#include <windows.h>
#include <tchar.h>
#include <conio.h>

using std::cout;
using std::endl;
using std::wstring;

int DeleteDirectory(const std::wstring &refcstrRootDirectory, bool bDeleteSubdirectories = true)
{
	bool            bSubdirectory = false;       // Flag, indicating whether
	// subdirectories have been found
	HANDLE          hFile;                       // Handle to directory
	std::wstring    strFilePath;                 // Filepath
	std::wstring    strPattern;                  // Pattern
	WIN32_FIND_DATA FileInformation;             // File information

	strPattern = refcstrRootDirectory + std::wstring("\\*.*");
	hFile = ::FindFirstFile(strPattern.c_str(), &FileInformation);
	if(hFile != INVALID_HANDLE_VALUE)
	{
		do
		{
			if(FileInformation.cFileName[0] != '.')
			{
				strFilePath.erase();
				strFilePath = refcstrRootDirectory + L"\\" + FileInformation.cFileName;

				if(FileInformation.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				{
					if(bDeleteSubdirectories)
					{
						// Delete subdirectory
						int iRC = DeleteDirectory(strFilePath, bDeleteSubdirectories);
						if(iRC)
							return iRC;
					}
					else
						bSubdirectory = true;
				}
				else
				{
					// Set file attributes
					if(::SetFileAttributes(strFilePath.c_str(),
						FILE_ATTRIBUTE_NORMAL) == FALSE)
						return ::GetLastError();

					// Delete file
					if(::DeleteFile(strFilePath.c_str()) == FALSE)
						return ::GetLastError();
				}
			}
		} while(::FindNextFile(hFile, &FileInformation) == TRUE);

		// Close handle
		::FindClose(hFile);

		DWORD dwError = ::GetLastError();
		if(dwError != ERROR_NO_MORE_FILES)
			return dwError;
		else
		{
			if(!bSubdirectory)
			{
				// Set directory attributes
				if(::SetFileAttributes(refcstrRootDirectory.c_str(),
					FILE_ATTRIBUTE_NORMAL) == FALSE)
					return ::GetLastError();

				// Delete directory
				if(::RemoveDirectory(refcstrRootDirectory.c_str()) == FALSE)
					return ::GetLastError();
			}
		}
	}
	return 0;
}
void remove(const wstring& str)
{
	int size = str.size();
	char* temp = new char[size + 1];
	for(int i = 0; i < size; ++i)
	{
		temp[i] = str[i];
	}
	temp[size] = NULL;
	remove(temp);
}


BehaviourOfDelete::BehaviourOfDelete(TotalCommander* tc):
	m_tc(tc), m_menu(new Menu("sdf", "sdf", "sdf", "sdf")){}
bool BehaviourOfDelete::Do()
{
	Window* wnd = m_tc->GetActiveWindow();
	m_tc->DeleteObserver(wnd);

	vector<pair<string, TYPE_OF_ELEMENT_TREE> >* ar = wnd->GetTree()->GetVectorOfSelectedElements();

	for(int i = 0, size = ar->size(); i < size; ++i)
	{
		m_menu->Draw("sdf", (*ar)[i].first);

		//���� ��� ���������� ������� ����
		if(m_menu->Run() == YES)
		{
			std::wstring strDirectoryToDelete; 

			string str = wnd->GetTree()->GetPath()->GetPath();
			str += '/';
			str += (*ar)[i].first;

			for(int j = 0, size = str.size(); j < size; ++j)
			{
				strDirectoryToDelete += (WCHAR)str[j];
			}

			if((*ar)[i].second == FOLDER_tree) DeleteDirectory(strDirectoryToDelete);
			else remove(strDirectoryToDelete);
		}
	}

	wnd->GetTree()->ClearPage();
	wnd->GetTree()->GoPath(*wnd->GetTree()->GetPath());
	wnd->GetTree()->IJustDeleteElement();
	wnd->Draw(REDRAW);

	m_tc->AddObserver(wnd);

	return true;
}