#ifndef BEHAVIOUROFMOVE_H
#define BEHAVIOUROFMOVE_H


#include "Command.h"
#include "TotalCommander.h"

//��������� ������ �� ���������
class BehaviourOfMove : public Command
{
	TotalCommander* m_tc;
	Window* m_lwnd;
	Window* m_rwnd;
public:
	BehaviourOfMove();
	explicit BehaviourOfMove(TotalCommander* tc);
	virtual bool Do();
};
#endif /* BEHAVIOUROFMOVE_H */
