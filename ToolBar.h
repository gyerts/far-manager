#ifndef TOOLBAR_H
#define TOOLBAR_H


#include "ConsoleLib.h"
#include "Coord.h"
#include "String.h"
#include "Observer.h"
using std::vector;

class ToolBar : public Observer
{
	Coord m_coord;
	String** m_str;
public:
	ToolBar(Coord crd) : m_coord(crd), m_str(new String*[4])
	{
		m_str[0] = new String(m_coord);
		m_str[0]->AddButton("F1",	"close left");
		m_str[0]->AddButton("F2",	"close rght");
		m_str[0]->AddButton("F4",	"new folder");
		m_str[0]->AddButton("TAB",	"activ");
		m_str[0]->AddButton("L CLICK",	"chng activ");
		m_str[0]->AddButton("DUBL CLICK",	"open");
		m_str[0]->AddButton("",	"");

		//shift
		m_str[1] = new String(m_coord);
		m_str[1]->AddButton("F5",	"copy");
		m_str[1]->AddButton("F6",	"paste");
		m_str[1]->AddButton("",	"");
		m_str[1]->AddButton("",	"");
		m_str[1]->AddButton("",	"");
		m_str[1]->AddButton("",	"");
		m_str[1]->AddButton("",	"");

		//ctrl
		m_str[2] = new String(m_coord);
		m_str[2]->AddButton("F8 ��� DEL",	"delete");
		m_str[2]->AddButton("",	"");
		m_str[2]->AddButton("",	"");
		m_str[2]->AddButton("",	"");
		m_str[2]->AddButton("",	"");
		m_str[2]->AddButton("L CLICK",	"select");
		m_str[2]->AddButton("",	"");

		m_str[3] = new String(m_coord);
	}
	void Draw(int index)
	{
		m_str[index]->Draw();
	}
	~ToolBar()
	{
		delete m_str[0];
		delete m_str[1];
		delete m_str[2];
		delete m_str[3];
		delete m_str;
	}
	virtual RESULT HandleEvent(const INPUT_RECORD events)
	{
		static DWORD ControlKeyState;
		static bool pressed = false;

		if(events.EventType == KEY_EVENT)
		{
			ControlKeyState = events.Event.KeyEvent.dwControlKeyState;

			if(events.Event.KeyEvent.bKeyDown)
			{
				if(ControlKeyState & SHIFT_PRESSED)
				{
					if(!pressed)
					{
						Draw(1);
						pressed = true;
					}
				}
				else if(ControlKeyState & LEFT_CTRL_PRESSED)
				{
					if(!pressed)
					{
						Draw(2);
						pressed = true;
					}
				}
			}
			else 
			{
				Draw(0);
				pressed = false;
			}
		}
		return NONE;
	}
};
#endif /* TOOLBAR_H */
