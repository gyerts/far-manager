#include "BehaviourOfMakeDir.h"
#include <iostream>
#include <string>
#include <cstring>
#include <sys/stat.h>
#include "ConsoleLib.h"
#include <conio.h>

using std::cout;
using std::endl;
using std::wcout;

bool CreateDirectoryTree(LPCTSTR szPathTree, LPSECURITY_ATTRIBUTES lpSecurityAttributes = NULL)
{
	bool bSuccess = false;
	const BOOL bCD = CreateDirectory(szPathTree, lpSecurityAttributes);

	DWORD dwLastError = 0;
	if(!bCD){
		dwLastError = GetLastError();
	}else{
		return true;
	}
	switch(dwLastError){
	case ERROR_ALREADY_EXISTS:
		bSuccess = true;
		break;
	case ERROR_PATH_NOT_FOUND:
		{
			TCHAR szPrev[MAX_PATH] = {0};
			LPCTSTR szLast = _tcsrchr(szPathTree,'\\');
			_tcsnccpy(szPrev,szPathTree,(int)(szLast-szPathTree));
			if(CreateDirectoryTree(szPrev,lpSecurityAttributes)){
				bSuccess = CreateDirectory(szPathTree,lpSecurityAttributes)!=0;
				if(!bSuccess){
					bSuccess = (GetLastError()==ERROR_ALREADY_EXISTS);
				}
			}else{
				bSuccess = false;
			}
		}
		break;
	default:
		bSuccess = false;
		break;
	}

	return bSuccess;
}

BehaviourOfMakeDir::BehaviourOfMakeDir(TotalCommander* tc):
	m_tc(tc){}

bool BehaviourOfMakeDir::Do()
{
	Tree* tree = m_tc->GetActiveWindow()->GetTree();

	//������� ������ ��� � ������ ����������
	tree->ICreateFolder();
	//����������� ������ � ����� ����������
	tree->Draw(ACTIVATED);

	//���� ����� ����, ��� ����� ��������� ��� ����� �����������
	int selector = tree->GetSelector();

	//��������� ����
	Coord crd = tree->GetCoord();
	GotoXY(crd.MoveDown(selector + 1));

	int size = tree->GetWidth();
	char strn[_MAX_PATH] = {}; 
	int index = 0;
	wchar_t ch;
	SetColor(LightRed, ConsoleColor(COLOR::BG_OF_WINDOW));

	while(true)
	{
		ch = _getch();

		if(ch == 13)
		{
			break;
		}
		else if(index == size)
		{
			break;
		}
		else 
		{
			strn[index] = ch;
		}
		wcout << strn[index++];
	}

	string str = tree->GetPath()->GetPath();
	wchar_t fin[_MAX_PATH] = {};

	int i = 0;
	int sizef = str.size();
	while(i < sizef)
	{
		char c = str[i];
		fin[i++] = c;
	}
	int j = 0;
	fin[i++] = '/';
	while(i < sizef + index + 1)
	{
		fin[i++] = strn[j++];
	}


	CreateDirectoryTree(fin);
	tree->ClearPage();
	tree->GoPath(*(tree->GetPath()));
	tree->Draw(ACTIVATED);
	return true;
}