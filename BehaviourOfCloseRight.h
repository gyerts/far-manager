#ifndef BEHAVIOUROFCLOSERIGHT_H
#define BEHAVIOUROFCLOSERIGHT_H


#include "Command.h"
#include "TotalCommander.h"

//��������� ������ �� ���������
class BehaviourOfCloseRight : public Command
{
	TotalCommander* m_tc;
	Window* m_lwnd;
	Window* m_rwnd;
public:
	explicit BehaviourOfCloseRight(TotalCommander* tc);
	virtual bool Do();
};
#endif /* BEHAVIOUROFCLOSERIGHT_H */
