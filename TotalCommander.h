#ifndef TOTALCOMMANDER_H
#define TOTALCOMMANDER_H


#include "Window.h"
#include <list>
#include <vector>
#include <utility>
#include "Command.h"
#include "Option.h"
#include "ToolBar.h"

using std::pair;
using std::list;
using std::vector;

class TotalCommander
{
	static TotalCommander*				m_instance;
	vector<pair<Command *, COMMAND> >	m_commands;
	list<Observer*>						m_observers;
	Window**							m_window;
	COORD								m_mouseCoord;
	ToolBar 							m_toolBar;

	vector<pair<string, TYPE_OF_ELEMENT_TREE> >*						m_buffer;
	string								m_buffer_path;

	//--------------------------------------Strategy-------------------------------------//
	Command*	GetCommandPtr(COMMAND cmd);
	//-----------------------------------Constructors------------------------------------//
	TotalCommander();//, m_toolBar(new ToolBar)
public:
	static TotalCommander* GetInstance();
	//-------------------------------------Observer--------------------------------------//
	void		AddObserver(Observer* observer);
	void		DeleteObserver(Observer* observer);
	void		NotifyObservers(const INPUT_RECORD events);
	//------------------------------------ENUMERATOR-------------------------------------//
	enum SIDE{LEFT, RIGHT};
	//----------------------------------GetterUserInput----------------------------------//
	void		LetsGetUsersInput();
	//--------------------------------------Strategy-------------------------------------//
	bool		DoCommand(COMMAND cmd);
	bool		AddCommand(Command* cmd, COMMAND enm);
	//---------------------------------------Set&Get-------------------------------------//
	Window*		GetWindow(SIDE sd);
	Window*		GetActiveWindow();

	void		Draw();
	COORD		GetMouseCoord();
	vector<pair<string, TYPE_OF_ELEMENT_TREE> >* GetBuffer();
	bool SetBuffer(vector<pair<string, TYPE_OF_ELEMENT_TREE> >* bf);
	bool CleanBuffer();
	void SetBufferPuth(const string& path);
	const string& GetBufferPuth();
	void SetWindows(Window* left, Window* right);
};
#endif /* TOTALCOMMANDER_H */
